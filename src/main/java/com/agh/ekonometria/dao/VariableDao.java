package com.agh.ekonometria.dao;

import com.agh.ekonometria.config.DbConfig;
import com.agh.ekonometria.dao.model.VariableDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mtomanski on 04.09.15.
 */
public class VariableDao {

    private static final String INSERT_SQL = "INSERT INTO Variable(name, sector_id) VALUES (?,?)";
    private static final String INSERT_SQL_WITH_TYPE = "INSERT INTO Variable(name, sector_id, type) VALUES (?,?,?)";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM Variable WHERE id = ?";
    private static final String SELECT_STATE_VARIABLE_BY_SECTOR_ID_SQL = "SELECT * FROM Variable WHERE sector_id = ? AND type = 0";
    private static final String SELECT_STATE_VARIABLE_BY_SECTOR_ID_TYPE_SQL = "SELECT * FROM Variable WHERE sector_id = ? AND type = ?";
    private static final String UPDATE_SQL = "UPDATE Variable SET name = ?, sector_id = ?, type = ? WHERE id = ?";
    private static final String DELETE_SQL = "DELETE FROM Variable WHERE id = ?";
    private static final String SELECT_LAST_ID = "SELECT id FROM Variable ORDER BY id DESC LIMIT 1";

    /*
     * Creates an entity in database
     */
    public static Integer create(String name, Integer sectorId) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setString(1, name);
            insertStm.setInt(2, sectorId);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public static Integer createWithType(String name, Integer sectorId, Integer type) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL_WITH_TYPE);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setString(1, name);
            insertStm.setInt(2, sectorId);
            insertStm.setInt(3, type);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /*
     * Returns an entity by id
     */
    public static VariableDb read(Integer id) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_ID_SQL)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new VariableDb(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /*
  * Returns a list of all entities associated to research of sectorID
  */
    public static List<VariableDb> readAllStateVariablesBySectorId(Integer sectorID) {
        Connection dbConnection = DbConfig.getConnection();
        List<VariableDb> variableDbs = new LinkedList<>();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_STATE_VARIABLE_BY_SECTOR_ID_SQL)) {
            preparedStatement.setInt(1, sectorID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                variableDbs.add(new VariableDb(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return variableDbs;
    }

    public static List<VariableDb> readAllTypeVariablesBySectorId(Integer sectorID, Integer type) {
        Connection dbConnection = DbConfig.getConnection();
        List<VariableDb> variableDbs = new LinkedList<>();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_STATE_VARIABLE_BY_SECTOR_ID_TYPE_SQL)) {
            preparedStatement.setInt(1, sectorID);
            preparedStatement.setInt(2, type);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                variableDbs.add(new VariableDb(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return variableDbs;
    }
    /*
     * Updates the entity of the provided object's id with its values
     */
    public static void update(VariableDb object) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setString(1, object.getName());
            preparedStatement.setInt(2, object.getSectorId());
            preparedStatement.setInt(3, object.getType());
            preparedStatement.setInt(4, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     * Deletes an entity of a given id
     */
    public static void delete(Integer id) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
