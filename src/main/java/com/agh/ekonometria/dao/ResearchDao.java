package com.agh.ekonometria.dao;

import com.agh.ekonometria.config.DbConfig;
import com.agh.ekonometria.dao.model.ResearchDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mtomanski on 04.09.15.
 */
public class ResearchDao {

    private static final String INSERT_SQL = "INSERT INTO Research(name, created_by) VALUES (?,?)";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM Research WHERE id = ?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM Research";
    private static final String UPDATE_SQL = "UPDATE Research SET name = ? WHERE id = ?";
    private static final String DELETE_SQL = "DELETE FROM Research WHERE id = ?";
    private static final String SELECT_LAST_ID = "SELECT id FROM Research ORDER BY id DESC LIMIT 1";

    /*
     * Creates an entity in database
     */
    public static Integer create(String name, String createdBy) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setString(1, name);
            insertStm.setString(2, createdBy);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /*
     * Returns an entity by id
     */
    public static ResearchDb read(Integer id) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_ID_SQL)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new ResearchDb(rs.getInt(1), rs.getString(2), rs.getString(3), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(4).getTime()), ZoneId.of("UTC")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /*
     * Returns a list of all entities
     */
    public static List<ResearchDb> readAll() {
        Connection dbConnection = DbConfig.getConnection();
        List<ResearchDb> researchDbs = new LinkedList<>();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_ALL_SQL)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                researchDbs.add(new ResearchDb(rs.getInt(1), rs.getString(2), rs.getString(3), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(4).getTime()), ZoneId.of("UTC"))));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return researchDbs;
    }

    /*
     * Updates the entity of the provided object's id with its values
     */
    public static void update(ResearchDb object) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setString(1, object.getName());
            preparedStatement.setInt(2, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     * Deletes an entity of a given id
     */
    public static void delete(Integer id) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
