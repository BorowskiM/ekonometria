package com.agh.ekonometria.dao;

import com.agh.ekonometria.config.DbConfig;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.TimeSeriesDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mtomanski on 04.09.15.
 */
public class TimeSeriesDao {

    private static final String INSERT_SQL = "INSERT INTO TimeSeries(variable_id, year, value) VALUES (?,?,?)";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM TimeSeries WHERE id = ?";
    private static final String SELECT_BY_VARIABLE_ID_SQL = "SELECT * FROM TimeSeries WHERE variable_id = ?";
    private static final String SELECT_BY_VARIABLE_ID_YEAR_SQL = "SELECT value FROM TimeSeries WHERE variable_id = ? AND year = ?";
    private static final String UPDATE_SQL = "UPDATE TimeSeries SET year = ?, value = ? WHERE id = ?";
    private static final String DELETE_SQL = "DELETE FROM TimeSeries WHERE id = ?";
    private static final String SELECT_LAST_ID = "SELECT id FROM TimeSeries ORDER BY id DESC LIMIT 1";

    /*
     * Creates an entity in database
     */
    public static Integer create(Integer variable_id, Integer year, Integer value) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setInt(1, variable_id);
            insertStm.setInt(2, year);
            insertStm.setDouble(3, value);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /*
     * Returns an entity by id
     */
    public static TimeSeriesDb read(Integer id) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_ID_SQL)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new TimeSeriesDb(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

  /*
  * Returns a list of all entities associated to research of researchId
  */
    public static List<TimeSeriesDb> readAllByVariableId(Integer variable_id) {
        Connection dbConnection = DbConfig.getConnection();
        List<TimeSeriesDb> timeseriesDbs = new LinkedList<>();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_VARIABLE_ID_SQL)) {
            preparedStatement.setInt(1, variable_id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                timeseriesDbs.add(new TimeSeriesDb(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4)));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return timeseriesDbs;
    }

    public static Double readValueByVariableId(Integer variable_id, Integer year) {
        Connection dbConnection = DbConfig.getConnection();
        Integer returnValue = null;

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_VARIABLE_ID_YEAR_SQL)) {
            preparedStatement.setInt(1, variable_id);
            preparedStatement.setInt(2, year);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            return rs.getDouble(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /*
     * Updates the entity of the provided object's id with its values
     */
    public static void update(TimeSeriesDb object) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setInt(1, object.getYear());
            preparedStatement.setDouble(2, object.getValue());
            preparedStatement.setInt(3, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     * Deletes an entity of a given id
     */
    public static void delete(Integer id) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
