package com.agh.ekonometria.dao;

import com.agh.ekonometria.config.DbConfig;
import com.agh.ekonometria.dao.model.SectorDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mtomanski on 04.09.15.
 */
public class SectorDao {

    private static final String INSERT_SQL = "INSERT INTO Sector(name, abbreviation, description, research_id) VALUES (?,?,?,?)";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM Sector WHERE id = ?";
    private static final String SELECT_BY_RESEARCH_ID_SQL = "SELECT * FROM Sector WHERE research_id = ?";
    private static final String UPDATE_SQL = "UPDATE Sector SET name = ?, abbreviation = ?, description = ?, research_id = ? WHERE id = ?";
    private static final String DELETE_SQL = "DELETE FROM Sector WHERE id = ?";
    private static final String SELECT_LAST_ID = "SELECT id FROM Sector ORDER BY id DESC LIMIT 1";

    /*
     * Creates an entity in database
     */
    public static Integer create(String name, String abbreviation, String description, Integer researchId) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setString(1, name);
            insertStm.setString(2, abbreviation);
            insertStm.setString(3, description);
            insertStm.setInt(4, researchId);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /*
     * Returns an entity by id
     */
    public static SectorDb read(Integer id) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_ID_SQL)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new SectorDb(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(5).getTime()), ZoneId.of("UTC")), rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

  /*
  * Returns a list of all entities associated to research of researchId
  */
    public static List<SectorDb> readAllByResearchId(Integer researchId) {
        Connection dbConnection = DbConfig.getConnection();
        List<SectorDb> researchDbs = new LinkedList<>();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_RESEARCH_ID_SQL)) {
            preparedStatement.setInt(1, researchId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                researchDbs.add(new SectorDb(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(5).getTime()), ZoneId.of("UTC")), rs.getInt(6)));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return researchDbs;
    }

    /*
     * Updates the entity of the provided object's id with its values
     */
    public static void update(SectorDb object) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setString(1, object.getName());
            preparedStatement.setString(2, object.getAbbreviation());
            preparedStatement.setString(3, object.getDescription());
            preparedStatement.setInt(4, object.getResearchId());
            preparedStatement.setInt(5, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     * Deletes an entity of a given id
     */
    public static void delete(Integer id) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
