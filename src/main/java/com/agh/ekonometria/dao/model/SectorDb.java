package com.agh.ekonometria.dao.model;

import java.time.LocalDateTime;

/**
 * Created by mtomanski on 04.09.15.
 */
public class SectorDb {

    // Autoincremented in database - no need for manual handling
    private Integer id;

    // Sector name
    private String name;

    // Sector abbreviation
    private String abbreviation;

    // Sector description
    private String description;

    // Inserted automatically by database while being created
    private LocalDateTime createdAt;

    // Foreign key to research
    private Integer researchId;

    public SectorDb(Integer id, String name, String abbreviation, String description, LocalDateTime createdAt, Integer researchId) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
        this.description = description;
        this.createdAt = createdAt;
        this.researchId = researchId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getResearchId() {
        return researchId;
    }

    public void setResearchId(Integer researchId) {
        this.researchId = researchId;
    }
}
