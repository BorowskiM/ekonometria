package com.agh.ekonometria.dao.model;

import java.time.LocalDateTime;

/**
 * Created by mtomanski on 04.09.15.
 */
public class ValueDb {

    private Integer id;

    private Integer variableId1;

    private Integer variableId2;

    private String value;

    private Boolean isQualityData;

    private String createdBy;

    private LocalDateTime createdAt;

    private String comment;

    private String certainty;

    private Integer scaleId;

    public ValueDb(Integer id, Integer variableId1, Integer variableId2, String value, Boolean isQualityData, String createdBy, LocalDateTime createdAt, String comment, String certainty, Integer scaleId) {
        this.id = id;
        this.variableId1 = variableId1;
        this.variableId2 = variableId2;
        this.value = value;
        this.isQualityData = isQualityData;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.comment = comment;
        this.certainty = certainty;
        this.scaleId = scaleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariableId1() {
        return variableId1;
    }

    public void setVariableId1(Integer variableId1) {
        this.variableId1 = variableId1;
    }

    public Integer getVariableId2() {
        return variableId2;
    }

    public void setVariableId2(Integer variableId2) {
        this.variableId2 = variableId2;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean isQualityData() {
        return isQualityData;
    }

    public void setIsQualityData(Boolean isQualityData) {
        this.isQualityData = isQualityData;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCertainty() {
        return certainty;
    }

    public void setCertainty(String certainty) {
        this.certainty = certainty;
    }

    public Integer getScaleId() {
        return scaleId;
    }

    public void setScaleId(Integer scaleId) {
        this.scaleId = scaleId;
    }
}
