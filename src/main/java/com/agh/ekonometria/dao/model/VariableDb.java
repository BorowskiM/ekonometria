package com.agh.ekonometria.dao.model;

/**
 * Created by mtomanski on 04.09.15.
 */
public class VariableDb {

    // Autoincremented in database - no need for manual handling
    private Integer id;

    // Research name
    private String name;

    // Name of the creator
    private Integer sectorId;

    private Integer type;

    public static int STATE_VARIABLE = 0;
    public static int INPUT_VARIABLE = 1;
    public static int OUTPUT_VARIABLE = 2;


    public VariableDb(Integer id, String name, Integer sectorId, Integer type) {
        this.id = id;
        this.name = name;
        this.sectorId = sectorId;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
