package com.agh.ekonometria.dao.model;

import java.time.LocalDateTime;

/**
 * Created by mtomanski on 04.09.15.
 * Bean representing Research row table
 */
public class ResearchDb {

    // Autoincremented in database - no need for manual handling
    private Integer id;

    // Research name
    private String name;

    // Name of the creator
    private String createdBy;

    // Inserted automatically by database while being created
    private LocalDateTime createdAt;

    public ResearchDb(Integer id, String name, String createdBy, LocalDateTime createdAt) {
        this.id = id;
        this.name = name;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
