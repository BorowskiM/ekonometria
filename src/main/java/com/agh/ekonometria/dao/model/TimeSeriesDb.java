package com.agh.ekonometria.dao.model;

import java.time.LocalDateTime;

/**
 * Created by mtomanski on 04.09.15.
 */
public class TimeSeriesDb {

    // Autoincremented in database - no need for manual handling
    private Integer id;

    // Variable id
    private Integer variable_id;

    // Year
    private Integer year;

    // Value
    private Double value;

    public TimeSeriesDb(Integer id, Integer variable_id, Integer year, Double value) {
        this.id = id;
        this.variable_id = variable_id;
        this.year = year;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public Integer getVariable_id() {
        return variable_id;
    }

    public Integer getYear() {
        return year;
    }

    public Double getValue() {
        return value;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setVariable_id(Integer variable_id) {
        this.variable_id = variable_id;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}