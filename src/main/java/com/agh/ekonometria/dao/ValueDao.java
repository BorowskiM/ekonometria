package com.agh.ekonometria.dao;

import com.agh.ekonometria.config.DbConfig;
import com.agh.ekonometria.dao.model.ValueDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by mtomanski on 04.09.15.
 */
public class ValueDao {

    private static final String INSERT_SQL = "INSERT INTO Value(variable_id_1, variable_id_2, value, is_quality_data, created_by, comment, certainty, scale_id) VALUES (?,?,?,?,?,?,?,?)";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM Value WHERE id = ?";
    private static final String SELECT_BY_VARIABLE_IDS_SQL = "SELECT * FROM Value WHERE (variable_id_1 = ? AND variable_id_2 = ?) OR (variable_id_1 = ? AND variable_id_2 = ?)";
    private static final String UPDATE_SQL = "UPDATE Value SET variable_id_1 = ?, variable_id_2 = ?, value = ?, is_quality_data = ?, comment = ?, certainty = ?, scale_id = ? WHERE id = ?";
    private static final String DELETE_SQL = "DELETE FROM Value WHERE id = ?";
    private static final String SELECT_LAST_ID = "SELECT id FROM Value ORDER BY id DESC LIMIT 1";

    /*
     * Creates an entity in database
     */
    public static Integer create(Integer variableId1, Integer variableId2, String value, Boolean isQualityData, String createdBy, String comment, String certainty, Integer scaleId) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement insertStm = dbConnection.prepareStatement(INSERT_SQL);
             PreparedStatement selectLastStm = dbConnection.prepareStatement(SELECT_LAST_ID)
        ){
            insertStm.setInt(1, variableId1);
            insertStm.setInt(2, variableId2);
            insertStm.setString(3, value);
            insertStm.setBoolean(4, isQualityData);
            insertStm.setString(5, createdBy);
            insertStm.setString(6, comment);
            insertStm.setString(7, certainty);
            insertStm.setInt(8, scaleId);
            insertStm.executeUpdate();
            ResultSet rs = selectLastStm.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    /*
     * Returns an entity by id
     */
    public static ValueDb read(Integer id) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_ID_SQL)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new ValueDb(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getBoolean(5), rs.getString(6), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(7).getTime()), ZoneId.of("UTC")), rs.getString(8), rs.getString(9), rs.getInt(10));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static ValueDb readByVariableIds(Integer variableId1, Integer variableId2) {
        Connection dbConnection = DbConfig.getConnection();
        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(SELECT_BY_VARIABLE_IDS_SQL)) {
            preparedStatement.setInt(1, variableId1);
            preparedStatement.setInt(2, variableId2);
            preparedStatement.setInt(3, variableId2);
            preparedStatement.setInt(4, variableId1);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new ValueDb(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getBoolean(5), rs.getString(6), LocalDateTime.ofInstant(Instant.ofEpochMilli(rs.getTimestamp(7).getTime()), ZoneId.of("UTC")), rs.getString(8), rs.getString(9), rs.getInt(10));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /*
     * Updates the entity of the provided object's id with its values
     */
    public static void update(ValueDb object) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setInt(1, object.getVariableId1());
            preparedStatement.setInt(2, object.getVariableId2());
            preparedStatement.setString(3, object.getValue());
            preparedStatement.setBoolean(4, object.isQualityData());
            preparedStatement.setString(5, object.getComment());
            preparedStatement.setString(6, object.getCertainty());
            preparedStatement.setInt(7, object.getScaleId());
            preparedStatement.setInt(8, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /*
     * Deletes an entity of a given id
     */
    public static void delete(Integer id) {
        Connection dbConnection = DbConfig.getConnection();

        try (PreparedStatement preparedStatement = dbConnection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
