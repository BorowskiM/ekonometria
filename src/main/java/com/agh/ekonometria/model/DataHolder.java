package com.agh.ekonometria.model;

import com.agh.ekonometria.views.SectorSelectLayout;
import com.vaadin.ui.RichTextArea;

/**
 * Created by Artur on 2015-09-05.
 */
public class DataHolder {
    private boolean isQualityData;
    private int selectTypeOfScale;
    private String value;
    private RichTextArea note;
    private String firstSectorName;
    private String secondSectorName;
    private String researchName;
    private Integer variableId;

    public DataHolder(boolean isQualityData, int selectTypeOfScale, String value, RichTextArea note,Integer variableId) {
        this.isQualityData = isQualityData;
        this.selectTypeOfScale = selectTypeOfScale;
        this.value = value;
        this.note = note;
        this.firstSectorName = getSectorName(0);
        this.secondSectorName = getSectorName(1);
        this.researchName = "Test";
        this.variableId = variableId;
    }

    private String getSectorName(int i) {
        String sectorName;
        try {
//            sectorName = SectorSelectLayout.getButtonSectorMap().get(SectorSelectLayout.getClickedButtons().get(i)).getName();
        } catch (Exception e) {
            return null;
        }
        return "";
    }

    public DataHolder(boolean isQualityData, int selectTypeOfScale, String value) {
        this(isQualityData, selectTypeOfScale, value, new RichTextArea(),null);
    }

    public DataHolder(boolean isQualityData, int selectTypeOfScale, String value, Integer variableId) {
        this(isQualityData, selectTypeOfScale, value, new RichTextArea(),variableId);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public RichTextArea getNote() {
        return note;
    }

    public void setNote(RichTextArea note) {
        this.note = note;
    }

    public String getFirstSectorName() {
        return firstSectorName;
    }

    public void setFirstSectorName(String firstSectorName) {
        this.firstSectorName = firstSectorName;
    }

    public String getSecondSectorName() {
        return secondSectorName;
    }

    public void setSecondSectorName(String secondSectorName) {
        this.secondSectorName = secondSectorName;
    }

    public String getResearchName() {
        return researchName;
    }

    public void setResearchName(String researchName) {
        this.researchName = researchName;
    }

    public boolean isQualityData() {
        return isQualityData;
    }

    public void setIsQualityData(boolean isQualityData) {
        this.isQualityData = isQualityData;
    }

    public int getSelectTypeOfScale() {
        return selectTypeOfScale;
    }

    public void setSelectTypeOfScale(int selectTypeOfScale) {
        this.selectTypeOfScale = selectTypeOfScale;
    }

    public Integer getVariableId()
    {
        return variableId;
    }

    public void setVariableId(Integer variableId)
    {
        this.variableId = variableId;
    }
}
