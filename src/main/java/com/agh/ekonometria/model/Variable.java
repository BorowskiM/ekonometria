/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.model;

public class Variable
{
    private String name;
    private String description;
    //beznadziejne potrzebne do GUI
    private Object variableId;
    private int variableIdDb;

    public String getName()
    {
        return name;
    }

    public Variable withName(String name)
    {
        this.name = name;
        return this;
    }

    public String getDescription()
    {
        return description;
    }
    public Object getVariableId()
    {
        return variableId;
    }
    public int getVariableIdDb()
    {
        return variableIdDb;
    }
    public Variable withDescription(String description)
    {
        this.description = description;
        return this;
    }
    public Variable withVariableId(Object variableId)
    {
        this.variableId = variableId;
        return this;
    }
    public Variable withVariableIdDb(int variableIdDb)
    {
        this.variableIdDb = variableIdDb;
        return this;
    }
}
