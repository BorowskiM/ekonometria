package com.agh.ekonometria.model;

import java.util.LinkedList;
import java.util.List;
import com.agh.ekonometria.dao.model.VariableDb;

/**
 * Created by Artur on 2015-07-05.
 */
public class Sector
{
    private Integer id;
    private String name;
    private String abbreviation;
    private String description;
    private List<VariableDb> variables = new LinkedList<>();

    public Sector()
    {
        this.id = 0;
        this.name = "";
        this.abbreviation = "";
        this.description = "";
    }

    public Sector(Integer id, String name, String abbreviation, String description)
    {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public Sector withName(String name)
    {
        this.name = name;
        return this;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public Sector withShortcutName(String abbreviation)
    {
        this.abbreviation = abbreviation;
        return this;
    }

    public String getDescription()
    {
        return description;
    }

    public Sector withDescription(String description)
    {
        this.description = description;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sector withId(Integer id)
    {
        this.id = id;
        return this;
    }

    public Sector withVariables(VariableDb... variables)
    {
        if (this.variables == null)
        {
            this.variables = new LinkedList<>();
        }
        if (variables != null)
        {
            for (int i = 0; i < variables.length; i++)
            {

                this.variables.add(variables[i]);
            }
        }
        return this;
    }

    public List<VariableDb> getVariables()
    {
        return variables;
    }

    public Sector withVariables(List<VariableDb> variableDbs)
    {
        variables.addAll(variableDbs);
        return this;
    }
}
