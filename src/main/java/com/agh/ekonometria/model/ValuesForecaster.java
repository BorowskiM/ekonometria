package com.agh.ekonometria.model;
//
//  OpenForecast - open source, general-purpose forecasting package.
//  Copyright (C) 2002-2011  Steven R. Gould
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

import net.sourceforge.openforecast.*;

import java.util.Iterator;

public class ValuesForecaster {
    public static void main(String[] args) {
        DataSet observedData = new DataSet();
        Double[] values = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.104, 2.305, 2.83, 5.63, 6.09, 6.68, 8.31, 9.511, 10.712, 11.913, 13.114, 14.315, 15.516
        };
        Double[] result = forecastValues(values, 3);

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    public static Double[] forecastValues(Double[] values, int howManyNextValues) {
        Double result[] = new Double[howManyNextValues];
        DataSet observedData = new DataSet();

        for (int count = 0; count < values.length; count++) {
            DataPoint dp = new Observation(values[count]);
            dp.setIndependentValue("x", (2015 - values.length + count));
            observedData.add(dp);
        }

        // Obtain a good forecasting model given this data set
        ForecastingModel forecaster = Forecaster.getBestForecast(observedData);

        // Create additional data points for which forecast values are required
        DataSet requiredDataPoints = new DataSet();
        for (int count = 1; count <= howManyNextValues; count++) {
            DataPoint dp = new Observation(0.0);
            dp.setIndependentValue("x", count + 2015);
            requiredDataPoints.add(dp);
        }

        DataSet forecast = forecaster.forecast(requiredDataPoints);

        //  forecast data points
        Iterator it = forecast.iterator();
        int i = 0;
        while (it.hasNext()) {
            DataPoint dp = (DataPoint) it.next();
            double forecastValue = dp.getDependentValue();

            if (forecastValue < 0) {
                forecastValue = 0;
            }
            // Do something with the forecast value, e.g.
            System.out.println(dp);
            result[i++] = Math.round(forecastValue * 100) / 100.0;
        }

        return result;
    }
}
