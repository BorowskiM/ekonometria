package com.agh.ekonometria.model;

import com.agh.ekonometria.dao.TimeSeriesDao;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

/**
 * Created by Artur on 2015-09-20.
 */
public class TimeSeriesTable extends Table {

    public TimeSeriesTable(int variableIdDb) {
        init(variableIdDb);
    }

    private void init(int variableIdDb) {

        this.addContainerProperty("Rok", String.class, null);

        for (int i = 0; i < 15; i++) {
            this.addContainerProperty(String.valueOf(1990 + i), String.class, null);
        }

        Object newItemId = this.addItem();
        Item row = this.getItem(newItemId);

        row.getItemProperty("Rok").setValue("Wartość");

        for (int i = 0; i < 15; i++) {
            Integer year = 1990 + i;
            Double value = TimeSeriesDao.readValueByVariableId(variableIdDb, year);

            if (value != null && value != 0) {
                row.getItemProperty(String.valueOf(year)).setValue(String.valueOf(value));
            } else {
                row.getItemProperty(String.valueOf(year)).setValue("");
            }
        }

        Object newItemId2 = this.addItem();
        Item row2 = this.getItem(newItemId2);

        row2.getItemProperty("Rok").setValue("Rok");

        for (int i = 0; i < 15; i++) {
            row2.getItemProperty(String.valueOf(1990 + i)).setValue(String.valueOf(2005 + i));
        }

        Object newItemId3 = this.addItem();
        Item row3 = this.getItem(newItemId3);

        row3.getItemProperty("Rok").setValue("Wartość");

        for (int i = 0; i < 15; i++) {
            Integer year = 2005 + i;
            Double value = TimeSeriesDao.readValueByVariableId(variableIdDb, year);

            if (value != null && value != 0) {
                row3.getItemProperty(String.valueOf(1990 + i)).setValue(String.valueOf(value));
            } else {
                row3.getItemProperty(String.valueOf(1990 + i)).setValue("");
            }
        }

        this.setSortEnabled(false);
        this.setPageLength(this.size());

    }

}
