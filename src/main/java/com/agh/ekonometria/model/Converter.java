/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.model;

import com.agh.ekonometria.Constants;
import com.agh.ekonometria.views.tablecell.HeaderVariableTextField;
import com.agh.ekonometria.views.tablecell.MyNativeSelect;
import com.agh.ekonometria.views.tablecell.QualityQuantityDataLayout;
import com.agh.ekonometria.views.tablecell.ScaleTypeOptionGroup;
import com.agh.ekonometria.views.tablecell.SingleCellLayout;
import com.vaadin.ui.*;
import org.apache.commons.collections.keyvalue.MultiKey;

import java.util.*;

public class Converter {
    public Map<MultiKey, DataHolder> retrieveDataFromTable(Table table) {
        Map<MultiKey, DataHolder> data = new LinkedHashMap<>();
        try {
            List<VariableKey> columnsKeys = getAllColumnHeaders(table);
            Iterator<Integer> iterator = getIterator(table);
            leaveFirstRaw(iterator);

            while (iterator.hasNext()) {
                Integer itemId = iterator.next();
                List<DataHolder> values = getRowValues(table, itemId);
                addValuesToDataMap(data, columnsKeys, itemId, values);
            }
        } catch (Exception e) {
            System.out.println("Exception");
        }
        return data;
    }

    private void leaveFirstRaw(Iterator<Integer> iterator) {
        iterator.next();
    }

    private void addValuesToDataMap(Map<MultiKey, DataHolder> data, List<VariableKey> columnsKeys, Integer itemId, List<DataHolder> valuesArray) {
        for (int i = 1; i < valuesArray.size(); i++) {
            VariableKey variableKey = new VariableKey();
            variableKey.setVariableId(valuesArray.get(0).getVariableId());
            variableKey.setVariableName(valuesArray.get(0).getValue());
            data.put(new MultiKey(variableKey, columnsKeys.get(i)), valuesArray.get(i));
        }
    }

    private List<DataHolder> getRowValues(Table table, Integer itemId) {
        List<DataHolder> rowValues = new ArrayList<>();
        Iterator iterator = table.getItem(itemId).getItemPropertyIds().iterator();
        if (iterator.hasNext()) {
            HeaderVariableTextField component = (HeaderVariableTextField) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(iterator.next()).getValue()).getComponent(0);
            rowValues.add(new DataHolder(false, Constants.HEADER_VALUE, component.getValue(),component.getVariableId()));
        }
        while (iterator.hasNext()) {
            Object it = iterator.next();
            com.vaadin.ui.RichTextArea text = ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getNote().getWindow().getRichTextArea();
            MyNativeSelect component = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQualityDataNativeSelect();
            if (isQualityData(component)) {
                int typeOfScale = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getTypeOfScale().getNumberTypeOfScale();
                rowValues.add(new DataHolder(true, typeOfScale, String.valueOf(component.getValue()), text,null));
            } else {
                TextField componentText = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQuantityTextField();
                rowValues.add(new DataHolder(false, Constants.QUANTITY_VALUE, String.valueOf(componentText.getValue()), text,null));
            }
        }
        return rowValues;
    }

    private boolean isQualityData(MyNativeSelect component) {
        return (component.isVisible());
    }

    private Iterator<Integer> getIterator(Table table) {
        return (Iterator<Integer>) table.getContainerDataSource().getItemIds().iterator();
    }

    private List<VariableKey> getAllColumnHeaders(Table table) {
        List<VariableKey> headersKeys = new ArrayList<>();
        Iterator<String> iterator = (Iterator<String>) table.getItem(0).getItemPropertyIds().iterator();
        while (iterator.hasNext()) {
            HeaderVariableTextField component = (HeaderVariableTextField) ((HorizontalLayout) (table).getItem(0).getItemProperty(iterator.next()).getValue()).getComponent(0);
            VariableKey variableKey = new VariableKey();
            variableKey.setVariableName(component.getValue());
            variableKey.setVariableId(component.getVariableId());
            headersKeys.add(variableKey);
        }
        return headersKeys;
    }
}
