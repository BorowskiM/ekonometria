/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.model;

import com.agh.ekonometria.views.tablecell.HeaderVariableTextField;
import com.agh.ekonometria.views.tablecell.MyNativeSelect;
import com.agh.ekonometria.views.tablecell.NotesWindow;
import com.agh.ekonometria.views.tablecell.QualityQuantityDataLayout;
import com.agh.ekonometria.views.tablecell.ScaleTypeOptionGroup;
import com.agh.ekonometria.views.tablecell.SingleCellLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.keyvalue.MultiKey;

public class TableDataFiller {
    private int numberOfItems;
    private Table table;

    public void fillTableWithProperData(Map<MultiKey, DataHolder> dataBeforeMap, Table table, int numberOfItems) {
        this.numberOfItems = numberOfItems;
        this.table = table;

        Set<MultiKey> multiKeys = dataBeforeMap.keySet();
        Set<String> verticalHeaderNames = new LinkedHashSet<>();
        Set<String> horizontalHeaderNames = new LinkedHashSet<>();
        Set<VariableKey> verticalHeadersKey = new LinkedHashSet<>();
        Set<VariableKey> horizontalHeadersKey = new LinkedHashSet<>();

        Iterator iterator = multiKeys.iterator();

        while (iterator.hasNext()) {
            MultiKey multiKey = (MultiKey) iterator.next();
            verticalHeaderNames.add(((VariableKey) multiKey.getKey(0)).getVariableName());
            horizontalHeaderNames.add(((VariableKey) multiKey.getKey(1)).getVariableName());

            verticalHeadersKey.add((VariableKey)multiKey.getKey(0));
            horizontalHeadersKey.add((VariableKey) multiKey.getKey(1));
        }
        try {
            fillHorizontalHeaders(new LinkedList<>(horizontalHeaderNames));
            fillVerticalHeaders(new LinkedList<>(verticalHeaderNames));
            fillCells(new LinkedList<>(verticalHeadersKey), new LinkedList<>(horizontalHeadersKey), dataBeforeMap);
        } catch (Exception e) {

        }
    }

    private void fillCells(LinkedList<VariableKey> verticalHeaders, LinkedList<VariableKey> horizontalHeaders, Map<MultiKey, DataHolder> dataBeforeMap) {
        for (int j = 1; j < numberOfItems - 1; j++) {
            for (int i = 1; i < numberOfItems - 1; i++) {
                MultiKey multiKey = new MultiKey(verticalHeaders.get(j - 1), horizontalHeaders.get(i - 1));
                if (dataBeforeMap.get(multiKey) != null) {
                    DataHolder cell = getNewValue(dataBeforeMap, multiKey);
                    NotesWindow note = ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getNote().getWindow();
                    MyNativeSelect slider = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQualityDataNativeSelect();
                    TextField componentText = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQuantityTextField();
                    ScaleTypeOptionGroup typeOfScale = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getTypeOfScale();
                    Button informationType = ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getNote().getInformationType();

                    note.setRichTextArea(cell.getNote());

                    if (cell.isQualityData()) {
                        slider.setValue(Double.valueOf(cell.getValue()));
                        typeOfScale.setValue(typeOfScale.getStringTypeOfScale(cell.getSelectTypeOfScale()));
                        slider.setVisible(true);
                        typeOfScale.setVisible(true);
                        componentText.setVisible(false);
                        informationType.setCaption("JAK");
                    } else {
                        slider.setVisible(false);
                        typeOfScale.setVisible(false);
                        componentText.setVisible(true);
                        componentText.setValue(cell.getValue());
                        informationType.setCaption("ILO");
                    }


//                    Object it = iterator.next();
//                    com.vaadin.ui.RichTextArea text = ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getNote().getWindow().getRichTextArea();
//                    MyNativeSelect component = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQualityDataNativeSelect();
//                    if (isQualityData(component)) {
//                        int typeOfScale = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getTypeOfScale().getNumberTypeOfScale();
//                        rowValues.add(new DataHolder(true, typeOfScale, String.valueOf(component.getValue()), text));
//                    } else {
//                        TextField componentText = ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(itemId).getItemProperty(it).getValue()).getComponent(0)).getComponent(0)).getComponent(0))).getQuantityTextField();
//                        rowValues.add(new DataHolder(false, Constants.QUANTITY_VALUE, String.valueOf(componentText.getValue()), text));
//                    }


//                            ((MyNativeSelect) ((QualityQuantityDataLayout) (((VerticalLayout) ((SingleCellLayout) ((HorizontalLayout) (table).getItem(j).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).getComponent(0)).getComponent(0)).setValue(Double.valueOf(getNewValue(dataBeforeMap, multiKey)));
                }
            }
        }
    }

    private DataHolder getNewValue(Map<MultiKey, DataHolder> dataBeforeMap, MultiKey multiKey) {
        return dataBeforeMap.get(multiKey);
    }

    private void fillVerticalHeaders(List<String> verticalHeaders) {
        for (int i = 1; i < numberOfItems - 1; i++) {
            ((HeaderVariableTextField) ((HorizontalLayout) table.getItem(i).getItemProperty(String.valueOf(0)).getValue()).getComponent(0)).setValue(verticalHeaders.get(i - 1));
        }
    }

    private void fillHorizontalHeaders(List<String> horizontalHeaders) {
        for (int i = 1; i < numberOfItems - 1; i++) {
            ((HeaderVariableTextField) ((HorizontalLayout) table.getItem(0).getItemProperty(String.valueOf(i)).getValue()).getComponent(0)).setValue(horizontalHeaders.get(i - 1));
        }
    }
}
