/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.model;

public class VariableKey
{
    private String variableName;

    private Integer variableId = null;

    public Integer getVariableId()
    {
        return variableId;
    }

    public void setVariableId(Integer variableId)
    {
        this.variableId = variableId;
    }

    public String getVariableName()
    {
        return variableName;
    }

    public void setVariableName(String variableName)
    {
        this.variableName = variableName;
    }
    @Override
    public int hashCode() {
        if (variableId != null)
        {
            return variableName.hashCode() + variableId.hashCode();
        }
        else
        {
            return variableName.hashCode();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof VariableKey))
            return false;
        if (obj == this)
            return true;

        return variableName.equals(((VariableKey) obj).getVariableName()) && variableId.equals(((VariableKey) obj).getVariableId());
    }

}
