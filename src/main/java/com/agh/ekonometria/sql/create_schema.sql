-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: sql2.freemysqlhosting.net
-- Czas wygenerowania: 10 Wrz 2015, 13:42
-- Wersja serwera: 5.5.43-0ubuntu0.12.04.1
-- Wersja PHP: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `sql288878`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Research`
--

CREATE TABLE IF NOT EXISTS `Research` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8 NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Sector`
--

CREATE TABLE IF NOT EXISTS `Sector` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8 NOT NULL,
  `abbreviation` varchar(20) NOT NULL,
  `description` varchar(4096) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `research_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sector_research` (`research_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;


-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Value`
--

CREATE TABLE IF NOT EXISTS `Value` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `variable_id_1` int(20) NOT NULL,
  `variable_id_2` int(20) NOT NULL,
  `value` text,
  `is_quality_data` tinyint(1) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(400) DEFAULT NULL,
  `certainty` varchar(100) DEFAULT NULL,
  `scale_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_value_variable_1` (`variable_id_1`),
  KEY `fk_value_variable_2` (`variable_id_2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1424 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Variable`
--

CREATE TABLE IF NOT EXISTS `Variable` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `sector_id` int(20) NOT NULL,
  `type` int(20) DEFAULT 0 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_variable_sector` (`sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=470 ;

--
-- Ograniczenia dla tabeli `Sector`
--
ALTER TABLE `Sector`
  ADD CONSTRAINT `fk_sector_research` FOREIGN KEY (`research_id`) REFERENCES `Research` (`id`);

--
-- Ograniczenia dla tabeli `Value`
--
ALTER TABLE `Value`
  ADD CONSTRAINT `fk_value_variable_1` FOREIGN KEY (`variable_id_1`) REFERENCES `Variable` (`id`),
  ADD CONSTRAINT `fk_value_variable_2` FOREIGN KEY (`variable_id_2`) REFERENCES `Variable` (`id`);

--
-- Ograniczenia dla tabeli `Variable`
--
ALTER TABLE `Variable`
  ADD CONSTRAINT `fk_variable_sector` FOREIGN KEY (`sector_id`) REFERENCES `Sector` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE IF NOT EXISTS `TimeSeries` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `variable_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `value` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_variable_id` (`variable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

ALTER TABLE Variable ADD type int(20) DEFAULT 0 NOT NULL;

select * from Variable;