/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class HeaderLayout extends VerticalLayout
{
    Label welcomeLabel = new Label("Model socjoekonometryczny");
    public HeaderLayout()
    {
        init();
        initComponents();
        addComponents();
    }

    private void init()
    {

    }

    private void initComponents()
    {
     welcomeLabel.setHeight(50F, Unit.PIXELS);
    }
    private void addComponents()
    {
        addComponent(welcomeLabel);
    }
}
