/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;

import com.vaadin.ui.*;

@SuppressWarnings("serial")
public class SelectQualityQuantityData extends VerticalLayout {

    private boolean isQualityDataForCell;

    private Button informationType;

    public SelectQualityQuantityData(boolean isQualityData, TextField quantityTextField, MyNativeSelect qualityDataNativeSelect, ScaleTypeOptionGroup typeOfScale, Button informationType) {
        this.isQualityDataForCell = isQualityData;
        this.informationType = informationType;
        setSpacing(true);

        quantityTextField.setSizeFull();
        qualityDataNativeSelect.setSizeFull();
        if (isQualityData) {
            quantityTextField.setVisible(false);
            qualityDataNativeSelect.setVisible(true);
            typeOfScale.setVisible(true);
            informationType.setCaption("JAK");
        } else {
            quantityTextField.setVisible(true);
            qualityDataNativeSelect.setVisible(false);
            typeOfScale.setVisible(false);
            informationType.setCaption("ILO");
        }

        informationType.addClickListener(new Button.ClickListener() {
                                             @Override
                                             public void buttonClick(Button.ClickEvent clickEvent) {
                                                 if (isQualityDataForCell) {
                                                     informationType.setCaption("JAK");
                                                     quantityTextField.setVisible(false);
                                                     quantityTextField.setValue("");
                                                     qualityDataNativeSelect.setVisible(true);
                                                     typeOfScale.setVisible(true);
                                                     isQualityDataForCell = false;
                                                 } else {
                                                     informationType.setCaption("ILO");
                                                     quantityTextField.setVisible(true);
                                                     qualityDataNativeSelect.setVisible(false);
//                                                     qualityDataNativeSelect.setValue(null);
                                                     typeOfScale.setVisible(false);
                                                     isQualityDataForCell = true;
                                                 }
                                             }
                                         }
        );
        informationType.setStyleName("qualityQuantityButtonStyle");
        addComponent(informationType);
    }

    public Button getInformationType() {
        return informationType;
    }

    public void setInformationType(Button informationType) {
        this.informationType = informationType;
    }

    @Override
    public void attach() {
        super.attach();
    }
}