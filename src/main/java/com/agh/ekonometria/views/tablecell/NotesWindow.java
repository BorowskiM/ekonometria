/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;


import com.vaadin.ui.Button;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class NotesWindow extends Window
{
    public Button getButtonWhichDescriptionShouldBeUpdated() {
        return buttonWhichDescriptionShouldBeUpdated;
    }

    private RichTextArea richTextArea = new RichTextArea();
    private Button zapiszButton = new Button("Zapisz");
    private VerticalLayout windowLayout = new VerticalLayout();


    private Button buttonWhichDescriptionShouldBeUpdated;
    public NotesWindow()
    {
        initWindow();
        initComponents();
        addListeners();
        fillLayout();
        setContent(windowLayout);
    }

    private void initWindow()
    {
        center();
        setDraggable(true);
    }

    private void fillLayout()
    {
        windowLayout.addComponent(richTextArea);
        windowLayout.addComponent(zapiszButton);
    }

    private void addListeners()
    {
        zapiszButton.addClickListener(event -> {
            buttonWhichDescriptionShouldBeUpdated.setDescription(richTextArea.getValue());
            close();

        });
    }

    private void initComponents()
    {
        richTextArea.setCaption("Wprowadź notatkę:");
    }

    public void setButtonWhichDescriptionShouldBeUpdated(Button buttonWhichDescriptionShouldBeUpdated)
    {
        this.buttonWhichDescriptionShouldBeUpdated = buttonWhichDescriptionShouldBeUpdated;
    }

    public RichTextArea getRichTextArea() {
        return richTextArea;
    }

    public void setRichTextArea(RichTextArea richTextArea) {
        this.richTextArea = richTextArea;
    }
}
