/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

@SuppressWarnings("serial")
public class ToolTip extends VerticalLayout {

    private static final String NOTE = "";

    private NotesWindow window = new NotesWindow();
    private ScaleWindow scaleWindow = new ScaleWindow();
    private Button informationType = new Button();

    public ToolTip(boolean isQualityData, TextField quantityTextField, MyNativeSelect qualityDataNativeSelect, ScaleTypeOptionGroup typeOfScale) {
        setSpacing(true);

        Button noteButton = new Button(NOTE, event ->
        {
            UI.getCurrent().addWindow(window);
        });
        noteButton.setIcon(FontAwesome.FILE_TEXT_O);
        window.setButtonWhichDescriptionShouldBeUpdated(noteButton);

//        Button changeScaleButton = new Button(typeOfScale.getValue().toString(), event ->
//        {
//            UI.getCurrent().addWindow(window);
//        });
//        changeScaleButton.setIcon(FontAwesome.FILE_EXCEL_O);

        addComponent(new VerticalLayout(typeOfScale, new HorizontalLayout(noteButton, new SelectQualityQuantityData(isQualityData, quantityTextField, qualityDataNativeSelect, typeOfScale, informationType))));
    }

    public NotesWindow getWindow() {
        return window;
    }

    public Button getInformationType() {
        return informationType;
    }

    public void setInformationType(Button informationType) {
        this.informationType = informationType;
    }

    @Override
    public void attach() {
        super.attach();
    }
}
