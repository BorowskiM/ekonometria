/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;

import com.vaadin.data.Property;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class SingleCellLayout extends HorizontalLayout
{
    private ToolTip note;
    private TextField quantityTextField;
    private MyNativeSelect qualityDataNativeSelect;
    private ScaleTypeOptionGroup typeOfScale;

    public SingleCellLayout()
    {
        initLayout();
        // setComponentAlignment(note, Alignment.MIDDLE_CENTER);
    }

    public void init(boolean isQualityData)
    {
        note = new ToolTip(isQualityData, quantityTextField, qualityDataNativeSelect, typeOfScale);
        showItemsOfScaleType();
        QualityQuantityDataLayout firstRow = new QualityQuantityDataLayout(isQualityData, quantityTextField, qualityDataNativeSelect, typeOfScale);
        addComponent(new VerticalLayout(firstRow, note));

    }

    private void initLayout()
    {
        setStyleName("mymargins");
        setMargin(new MarginInfo(true, true, true, false));
        setSpacing(true);
    }

    private void showItemsOfScaleType()
    {
        typeOfScale.addValueChangeListener(new Property.ValueChangeListener()
        {
            public void valueChange(Property.ValueChangeEvent event)
            {
                // qualityDataNativeSelect.removeAllItems();
                qualityDataNativeSelect.setMin(0);
                switch (typeOfScale.getValue().toString())
                {
                    case "Pięciostopniowa":
                        qualityDataNativeSelect.setMax(4);
                        // qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Nie mam zdania");
                        // qualityDataNativeSelect.addItem("Raczej się zgadzam");
                        // qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                        break;
                    case "Czterostopniowa":
                        qualityDataNativeSelect.setMax(3);
                        // qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Raczej się zgadzam");
                        // qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                        break;
                    case "Siedmiostopniowa":
                        qualityDataNativeSelect.setMax(6);
                        // qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Nie zgadzam się");
                        // qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Nie mam zdania");
                        // qualityDataNativeSelect.addItem("Raczej się zgadzam");
                        // qualityDataNativeSelect.addItem("Zgadzam się");
                        // qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                        break;
                    default:
                        qualityDataNativeSelect.setMax(4);
                        // qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
                        // qualityDataNativeSelect.addItem("Nie mam zdania");
                        // qualityDataNativeSelect.addItem("Raczej się zgadzam");
                        // qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                }
            }
        });
    }

    public ToolTip getNote()
    {
        return note;
    }

    public SingleCellLayout withMyNativeSelect(MyNativeSelect myNativeSelect)
    {
        this.qualityDataNativeSelect = myNativeSelect;
        return this;
    }

    public SingleCellLayout withTextField(TextField quantityTextField)
    {
        this.quantityTextField = quantityTextField;
        return this;
    }
    public SingleCellLayout withScaleTypeOptionGroup(ScaleTypeOptionGroup scaleTypeOptionGroup)
    {
        this.typeOfScale = scaleTypeOptionGroup;
        return this;
    }
}
