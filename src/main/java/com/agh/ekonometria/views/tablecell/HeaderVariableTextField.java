/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;

import com.vaadin.ui.TextField;

public class HeaderVariableTextField extends TextField
{
    private Integer variableId;

    public HeaderVariableTextField()
    {
    }

    public Integer getVariableId()
    {
        return variableId;
    }

    public void setVariableId(Integer variableId)
    {
        this.variableId = variableId;
    }

}
