/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.tablecell;

import com.vaadin.ui.Slider;

public class MyNativeSelect extends Slider
{

    public MyNativeSelect()
    {
        init();
    }

    private void init()
    {
        setImmediate(true);
    }
}
