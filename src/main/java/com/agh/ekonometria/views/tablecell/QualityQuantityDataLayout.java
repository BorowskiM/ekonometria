package com.agh.ekonometria.views.tablecell;

import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class QualityQuantityDataLayout extends VerticalLayout {

    private final TextField quantityTextField;
    private final MyNativeSelect qualityDataNativeSelect;
    private final ScaleTypeOptionGroup typeOfScale;

    public QualityQuantityDataLayout(boolean isQualityData, TextField quantityTextField, MyNativeSelect qualityDataNativeSelect, ScaleTypeOptionGroup typeOfScale) {
        this.quantityTextField = quantityTextField;
        this.qualityDataNativeSelect = qualityDataNativeSelect;
        this.typeOfScale = typeOfScale;
        addComponent(qualityDataNativeSelect);
        addComponent(quantityTextField);
    }

    public TextField getQuantityTextField() {
        return quantityTextField;
    }

    public MyNativeSelect getQualityDataNativeSelect() {
        return qualityDataNativeSelect;
    }

    public ScaleTypeOptionGroup getTypeOfScale() {
        return typeOfScale;
    }
}
