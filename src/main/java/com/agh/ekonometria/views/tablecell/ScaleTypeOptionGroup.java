package com.agh.ekonometria.views.tablecell;

import com.agh.ekonometria.Constants;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;

/**
 * Created by Artur on 2015-09-06.
 */
public class ScaleTypeOptionGroup extends NativeSelect {

    public ScaleTypeOptionGroup(Object value, MyNativeSelect qualityDataNativeSelect) {
        //setCaption("Podaj typ skali Likerta:");

        addItems("Pięciostopniowa", "Czterostopniowa", "Siedmiostopniowa");
//        setStyleName("scaleTypeOptionGroup");
        setMultiSelect(false);
        setImmediate(true);
        setValue(value);

        switch (value.toString()) {
            case "Pięciostopniowa":
                qualityDataNativeSelect.setMax(4);
//                        qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Nie mam zdania");
//                        qualityDataNativeSelect.addItem("Raczej się zgadzam");
//                        qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                break;
            case "Czterostopniowa":
                qualityDataNativeSelect.setMax(3);
//                        qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Raczej się zgadzam");
//                        qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                break;
            case "Siedmiostopniowa":
                qualityDataNativeSelect.setMax(6);
//                        qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Nie zgadzam się");
//                        qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Nie mam zdania");
//                        qualityDataNativeSelect.addItem("Raczej się zgadzam");
//                        qualityDataNativeSelect.addItem("Zgadzam się");
//                        qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
                break;
            default:
                qualityDataNativeSelect.setMax(4);
//                        qualityDataNativeSelect.addItem("Całkowicie się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Raczej się nie zgadzam");
//                        qualityDataNativeSelect.addItem("Nie mam zdania");
//                        qualityDataNativeSelect.addItem("Raczej się zgadzam");
//                        qualityDataNativeSelect.addItem("Całkowicie się zgadzam");
        }
    }

    public int getNumberTypeOfScale() {
        switch (this.getValue().toString()) {
            case "Pięciostopniowa":
                return Constants.FIVE_GRADE;
            case "Czterostopniowa":
                return Constants.FOUR_GRADE;
            case "Siedmiostopniowa":
                return Constants.SEVEN_GRADE;
            default:
                return Constants.QUANTITY_VALUE;
        }
    }

    public String getStringTypeOfScale(int scale) {
        switch (scale) {
            case Constants.FIVE_GRADE:
                return "Pięciostopniowa";
            case Constants.FOUR_GRADE:
                return "Czterostopniowa";
            case Constants.SEVEN_GRADE:
                return "Siedmiostopniowa";
            default:
                return "Wybierz skalę";
        }
    }
}
