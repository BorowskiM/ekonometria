/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.ResearchDao;
import com.agh.ekonometria.views.optimalisation.OptimisationLayout;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ResearchDetailsLayout extends VerticalLayout
{
    private Button nextButton;
    private Button previousButton;
    private Button showAllTimeSeries;
    private Button executeOptimalisationProblem;
    private Label title;

    public ResearchDetailsLayout(String researchId)
    {
        setSpacing(true);
        initComponents();
        initListeners(researchId);
        addAllComponents(researchId);
    }

    private void initComponents() {
        nextButton = new Button("Dalej");
        nextButton.setIcon(FontAwesome.ARROW_CIRCLE_O_RIGHT);
        previousButton = new Button("Wstecz");
        previousButton.setIcon(FontAwesome.ARROW_CIRCLE_O_LEFT);
        showAllTimeSeries = new Button("Pokaż szeregi");
        showAllTimeSeries.setIcon(FontAwesome.PRINT);
        executeOptimalisationProblem = new Button("Dokonaj optymalizacji");
    }

    private void initListeners(String researchId) {
        nextButton.addClickListener(event -> {
            removeAllComponents();
            addComponent(new TableWithDataLayout(researchId));

        });
        previousButton.addClickListener(event -> {
            removeAllComponents();
            addComponent(new ResearchLayout());
        });
        showAllTimeSeries.addClickListener((Button.ClickListener) event -> {
            removeAllComponents();
            addComponent(new AllTimeSeriesLayout(researchId));
        });
        executeOptimalisationProblem.addClickListener((Button.ClickListener) event -> {
            removeAllComponents();
            addComponent(new OptimisationLayout(researchId));
        });
    }

    private void addAllComponents(String researchId)
    {
        title = new Label(ResearchDao.read(Integer.valueOf(researchId)).getName());
        title.setStyleName("title");
        addComponent(title);
        addComponent(new AddingSectorsAndVariablesLayout(researchId));
        addComponent(new HorizontalLayout(previousButton,nextButton,showAllTimeSeries,executeOptimalisationProblem));
    }
}
