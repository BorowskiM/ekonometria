/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.model.Sector;
import com.vaadin.ui.*;

public class AddVariableToSectorWindow extends Window
{
    private TextField variable = new TextField();
    private ComboBox comboBox = new ComboBox();
    private RichTextArea description = new RichTextArea();
    private Button zapiszButton = new Button("Zapisz");

    private Sector sectorToEdit = new Sector();
    private VerticalLayout windowLayout = new VerticalLayout();

    private Button buttonWhichDescriptionShouldBeUpdated;

    public AddVariableToSectorWindow()
    {
        setStyleName("upload-info");
        setDraggable(true);
        initComponents();
        addListeners();
        fillLayout();
        setContent(windowLayout);
    }

    private void fillLayout()
    {
        windowLayout.addComponent(variable);
        windowLayout.addComponent(comboBox);
        windowLayout.addComponent(description);
        windowLayout.addComponent(zapiszButton);
    }

    private void addListeners()
    {
        zapiszButton.addClickListener(event -> {
            if (buttonWhichDescriptionShouldBeUpdated != null)
            {
                buttonWhichDescriptionShouldBeUpdated.setDescription(getVariableName());
            }
            close();

        });
    }

    public String getVariableName()
    {
        return variable.getValue();
    }
    public Integer getVariableTypeNumber()
    {
        return ((VariableType)comboBox.getValue()).getNumber();
    }

    public String getDescription()
    {
        return description.getValue();
    }

    private void initComponents()
    {
        variable.setCaption("Podaj nazwę zmiennej:");
        description.setCaption("Podaj opis zmiennej:");
        comboBox.addItem(VariableType.STATE);
        comboBox.addItem(VariableType.INPUT);
        comboBox.addItem(VariableType.OUTPUT);
        comboBox.setValue(VariableType.STATE);
    }

    public void setButtonWhichNameShouldBeUpdated(Button buttonWhichDescriptionShouldBeUpdated)
    {
        this.buttonWhichDescriptionShouldBeUpdated = buttonWhichDescriptionShouldBeUpdated;
    }

    public Sector getSectorToEdit()
    {
        return sectorToEdit;
    }

    public void setSectorToEdit(Sector sectorToEdit)
    {
        this.sectorToEdit = sectorToEdit;
        variable.setValue(sectorToEdit.getName());
        description.setValue(sectorToEdit.getDescription());

    }
}
