/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

import java.util.List;

public class AllTimeSeriesLayout extends VerticalLayout {
    private Button previousButton;
    private VerticalLayout timeSeriesLayout;

    public AllTimeSeriesLayout(String reasearchId) {
        setSpacing(true);
        initComponents(reasearchId);
        initListeners(reasearchId);
        addAllComponents();
    }

    private void initComponents(String researchId) {
        previousButton = new Button("Wstecz");
        previousButton.setIcon(FontAwesome.ARROW_CIRCLE_O_LEFT);
        timeSeriesLayout = new VerticalLayout();

        List<SectorDb> sectorsFromDba = SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        for (SectorDb sectorDb : sectorsFromDba) {
            List<VariableDb> variables = VariableDao.readAllStateVariablesBySectorId(sectorDb.getId());
            if (variables != null && !variables.isEmpty()) {
                timeSeriesLayout.addComponent(new Label(sectorDb.getName()));
                for (VariableDb variableDb : variables) {
                    timeSeriesLayout.addComponent(new VariableShowTimeSeriesVerticalLayout(variableDb, variableDb.getId()));
                    timeSeriesLayout.addComponent(new Label(""));
                }
                timeSeriesLayout.addComponent(new Label(""));
            }
        }
    }

    private void initListeners(String researchId) {
        previousButton.addClickListener(event -> {
            removeAllComponents();
            addComponent(new ResearchDetailsLayout(researchId));
        });
    }

    private void addAllComponents() {
        addComponent(timeSeriesLayout);
        addComponents(new HorizontalLayout(previousButton));
    }
}
