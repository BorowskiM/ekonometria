/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.model.Sector;
import com.agh.ekonometria.views.buttons.AddVariableButton;
import com.agh.ekonometria.views.buttons.RemoveSectorButton;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeTable;
import java.util.List;

public class ButtonsPanel extends HorizontalLayout
{
    private AddVariableButton addVariableButton = new AddVariableButton();
    private RemoveSectorButton removeSectorButton = new RemoveSectorButton();
    private Sector sector;
    private Object sectorId;
    private int sectorIdDb;
    private TreeTable treeTable;
    private List<Sector> sectors;

    public ButtonsPanel()
    {
        addComponent(addVariableButton);
        addComponent(removeSectorButton);
    }

    public void init()
    {
        addVariableButton.setSectorIdInTable(sectorId);
        addVariableButton.setSector(sector);
        addVariableButton.setTreeTable(treeTable);
        addVariableButton.setSectorIdDb(sectorIdDb);
        removeSectorButton.withSector(sector).withSectorIdInTable(sectorId).withTreeTable(treeTable).withSectors(sectors).withSectorIdDb(sectorIdDb);
    }

    public ButtonsPanel withSectorIdInTable(Object sectorId)
    {
        this.sectorId = sectorId;
        return this;
    }

    public ButtonsPanel withTreeTable(TreeTable treeTable)
    {
        this.treeTable = treeTable;
        return this;
    }

    public ButtonsPanel withSector(Sector sector)
    {
        this.sector = sector;
        return this;
    }

    public ButtonsPanel withSectors(List<Sector> sectors)
    {
        this.sectors = sectors;
        return this;
    }

    public ButtonsPanel withSectorIdDb(int sectorIdDb)
    {
        this.sectorIdDb = sectorIdDb;
        return this;
    }
}
