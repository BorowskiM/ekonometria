/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views.optimalisation;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import java.util.ArrayList;
import java.util.List;

public class MatrixCLayout extends VerticalLayout {
    private String researchId;

    public MatrixCLayout(String researchId)
    {
        this.researchId = researchId;
        List<SectorDb> sectors = SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        List<VariableDb> outputVariables = new ArrayList<>();
        List<VariableDb> stateVariables = new ArrayList<>();

        for(SectorDb sector: sectors) {
            outputVariables.addAll(VariableDao.readAllTypeVariablesBySectorId(sector.getId(), VariableDb.OUTPUT_VARIABLE));
            stateVariables.addAll(VariableDao.readAllTypeVariablesBySectorId(sector.getId(), VariableDb.STATE_VARIABLE));
        }

        Table table = new Table("Macierz C");
        table.addContainerProperty("",String.class,null);
        stateVariables.stream().forEach(variable -> table.addContainerProperty(variable.getName(),Double.class,null));
        table.setEditable(false);
        table.setSizeFull();
       // for(VariableDb variableDb: outputVariables) {
            table.addItem(new Object[]{"Odsetek zatrudnionych w gospodarce posiadających specjalistyczne umiejętności ICT.(2011 zmiana metody …)"
                    ,        -0.24,2.31,1.37,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}, 1);
        table.addItem(new Object[]{"Udział zatrudnionych w sektorze ICT (produkcja) w zatrudnionych ogółem w gospodarce."
                    ,        0.0,0.42,0.08,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}, 2);
        table.addItem(new Object[]{"Udział zatrudnionych w sektorze ICT (produkcja) w zatrudnionych ogółem w gospodarce."
                ,        0.0,-1.50,0.64,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}, 3);
        table.addItem(new Object[]{"Wartość inwestycji BIZ w Polsce."
                ,        0.0,0.0,0.0,0.0,0.0,0.0,-3.61,6.05,113.03,0.0,0.0,0.0,0.0,0.0}, 4);
        table.addItem(new Object[]{"Ranking międzynarodowej konkurencyjności MDI (wartość = 1 najlepsze,artość = 100 najgorsze miejsce)"
                ,        0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,-0.13,0.0,0.0,0.0,0.0,0.0}, 5);

        table.setPageLength(table.size());
        addComponent(table);
    }
}
