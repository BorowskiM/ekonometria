/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views.optimalisation.matrixes;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

public class MatrixOptimize extends GridLayout {
    public MatrixOptimize(double[][] values) {
        int height = values[0].length;
        int width = values.length;
        setColumns(width);
        setRows(height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Label component = new Label(String.valueOf(values[i][j]));

                if (width < 8) {
                    component.setWidth(180, Sizeable.Unit.PIXELS);
                } else {
                    component.setWidth(80, Sizeable.Unit.PIXELS);
                }
                component.setHeight(40, Sizeable.Unit.PIXELS);

                addComponent(component, i, j);
            }
        }
    }
}
