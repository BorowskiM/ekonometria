/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views.optimalisation.matrixes;

import com.agh.ekonometria.dao.model.VariableDb;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

import java.util.List;

public class MatrixA extends GridLayout {
    public MatrixA(double[][] values, List<VariableDb> allVariables) {
        int height = values[0].length + 1;
        int width = values.length + 1;
        setColumns(width);
        setRows(height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Label component = null;
                if (i == 0 && j ==0) {
                    component = new Label("");
                    component.setWidth(400, Sizeable.Unit.PIXELS);
                    component.setHeight(600,Sizeable.Unit.PIXELS);

                }
                else if (i ==0)
                {
                    String name = allVariables.get(j-1).getName();
                    name = tuneString(name);
                    component = new Label(name);
                    component.setWidth(400, Sizeable.Unit.PIXELS);
                }
                else if (j == 0)
                {
                    String name = allVariables.get(i-1).getName();
                    name = tuneString(name);
                    component = new Label(name);
                    component.setWidth(180, Sizeable.Unit.PIXELS);
                    component.setHeight(600,Sizeable.Unit.PIXELS);

                }
                else {
                     component= new Label(String.valueOf(values[i-1][j-1]));
                    if (width < 8) {
                        component.setWidth(180, Sizeable.Unit.PIXELS);
                    } else {
                        component.setWidth(80, Sizeable.Unit.PIXELS);
                    }
                }
                component.setHeight(40, Sizeable.Unit.PIXELS);

                addComponent(component, i, j);
            }
        }
    }

    private String tuneString(String name) {
        if (name.length() >30)
        {
           return name.substring(0,30);
        }
        return name;
    }
}
