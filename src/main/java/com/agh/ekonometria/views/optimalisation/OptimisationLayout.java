/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views.optimalisation;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.TimeSeriesDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.ValueDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.matrix.Matrix;
import com.agh.ekonometria.views.ResearchDetailsLayout;
import com.agh.ekonometria.views.optimalisation.matrixes.MatrixA;
import com.agh.ekonometria.views.optimalisation.matrixes.MatrixOptimize;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;

import static com.agh.ekonometria.dao.ValueDao.readByVariableIds;

public class OptimisationLayout extends VerticalLayout {
    private Button showA = new Button("Pokaz macierz A");
    private Button showB = new Button("Pokaz macierz B");
    private Button prediction = new Button("Prognozuj");
    private Button optimisation = new Button("Optymalizuj");

    private Button previousButton;
    private static String researchId;
    private int size;
    private List<SectorDb> sectorsFromDba;
    private List<VariableDb> allVariables;

    public OptimisationLayout(String researchId) {
        this.researchId = researchId;
        sectorsFromDba = SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        allVariables = getAllVariables(sectorsFromDba);
        initComponents();
        addListeners();
        setSpacing(true);
        addComponents(new HorizontalLayout(showA, showB), new HorizontalLayout(prediction, optimisation), previousButton);
        addComponents(new HorizontalLayout(showA, showB), new MatrixCLayout(researchId), new HorizontalLayout(prediction, optimisation, previousButton));
    }

    private void addListeners() {
        int size = 50;
        Double[][] values = new Double[size][size];
        generateRandomValues(size, values);
        showA.addClickListener((Button.ClickListener) event -> {
            showB.setEnabled(true);
            if (ValuesContainer.matrixA == null) {
                ValuesContainer.matrixA = createMatrixA();
            }
            Window window = new Window("Stworz Macierz A:", new MatrixA(ValuesContainer.matrixA, allVariables));
            tuneWindow(window);
            showWindow(window);
        });
        showB.addClickListener((Button.ClickListener) event -> {
            Window window = new Window("Macierz B:", new MatrixOptimize(createMatrixB()));
            tuneWindow(window);
            showWindow(window);
        });

        prediction.addClickListener((Button.ClickListener) event -> {
            Window window = new Window("Prognoza:", new ForecastLayout(Integer.valueOf(researchId)));
            window.setClosable(true);
            window.setDraggable(true);
            window.setCloseShortcut(27);
            window.setImmediate(true);
            window.setSizeFull();
            showWindow(window);
        });
        previousButton.addClickListener(event -> {
            removeAllComponents();
            addComponent(new ResearchDetailsLayout(researchId));
        });

    }

    private double[][] createMatrixC() {

        int size = allVariables.size();

        double[][] matrixCValues = new double[size][1];

        for (int i = 0; i < size; i++) {
            int val = (int) (Math.random() * 2);
            matrixCValues[i][0] = Double.valueOf(val);
        }

        return matrixCValues;
    }

    private double[][] createMatrixB() {

        Matrix A = new Matrix(ValuesContainer.matrixA);
        double[] u = new double[A.getColumnDimension()];
        double[][] x2013 = new double[A.getColumnDimension()][1];
        double[][] x2014 = new double[A.getColumnDimension()][1];
        int index = 0;

        for (SectorDb sector : SectorDao.readAllByResearchId(Integer.valueOf(researchId))) {
            List<VariableDb> inputValues = VariableDao.readAllTypeVariablesBySectorId(sector.getId(), VariableDb.INPUT_VARIABLE);
            List<VariableDb> stateValues = VariableDao.readAllTypeVariablesBySectorId(sector.getId(), VariableDb.STATE_VARIABLE);
            Double ret = 1.0;

            for (VariableDb var : inputValues) {
                ret = TimeSeriesDao.readValueByVariableId(var.getId(), 2013);
                if (ret == null) {
                    ret = 1.0;
                }
            }

            for (VariableDb var : stateValues) {
                x2013[index][0] = (TimeSeriesDao.readValueByVariableId(var.getId(), 2013));
                x2014[index][0] = (TimeSeriesDao.readValueByVariableId(var.getId(), 2014));
                u[index] = ret;
                index++;
            }
        }

        Matrix x_t = new Matrix(x2013);
        Matrix x_tplus1 = new Matrix(x2014);

        Matrix B = x_tplus1.minus(A.times(x_t));

        double[][] B_val = new double[B.getColumnDimension()][B.getRowDimension()];

        B_val = B.getArray();

        for (int x = 0; x < B.getRowDimension(); x++) {
            for (int y = 0; y < B.getColumnDimension(); y++) {
                B_val[x][y] = B_val[x][y] / u[x];
            }
        }

        if (B_val == null || B_val.length == 0)//empty or unset array, nothing do to here
            return B_val;

        int width = B_val.length;
        int height = B_val[0].length;

        double[][] B_val_new = new double[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                B_val_new[y][x] = Math.round(B_val[x][y] * 100) / 100.0;
            }
        }
        return B_val_new;
    }


    private List<VariableDb> getOutputVariables(List<SectorDb> sectorsFromDba) {
        List<VariableDb> allVariables = new ArrayList<>();

        for (SectorDb sectorDb : sectorsFromDba) {
            allVariables.addAll(VariableDao.readAllTypeVariablesBySectorId(sectorDb.getId(), VariableDb.OUTPUT_VARIABLE));
        }
        return allVariables;
    }

    private double[][] createMatrixA() {
        sectorsFromDba = SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        allVariables = getAllVariables(sectorsFromDba);
        int size = allVariables.size();
        double[][] matrixAValues = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (allVariables.get(i).getName().equals(allVariables.get(j).getName())) {
                    matrixAValues[i][j] = 1.0;
                } else {
                    ValueDb valueDb = readByVariableIds(allVariables.get(i).getId(), allVariables.get(j).getId());
                    if (valueDb == null) {
                        matrixAValues[i][j] = 0.0;
                    } else if (valueDb.isQualityData()) {
                        matrixAValues[i][j] = getScaledValue(valueDb);
                    } else {
                        matrixAValues[i][j] = Double.valueOf(valueDb.getValue()) / 100;
                    }
                }
            }
        }
        return matrixAValues;
    }

    private static double getScaledValue(ValueDb valueDb) {
        if (valueDb.getScaleId().equals(1)) {
            return (Double.valueOf(valueDb.getValue()) / 5);
        }
        if (valueDb.getScaleId().equals(2)) {
            return (Double.valueOf(valueDb.getValue()) / 4);
        }
        if (valueDb.getScaleId().equals(3)) {
            return (Double.valueOf(valueDb.getValue()) / 7);
        }
        return 0.0;
    }

    private void generateRandomValues(int size, Double[][] values) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                values[i][j] = 1.0;
            }
        }
    }

    private void showWindow(Window window) {

        UI.getCurrent().addWindow(window);
//        UI.getCurrent().setFocusedComponent(window);
    }

    private void tuneWindow(Window window) {
        window.setClosable(true);
        window.setDraggable(true);
        window.setCloseShortcut(27);
        window.setImmediate(true);
        window.setSizeFull();
    }

    private void initComponents() {
        showA.setIcon(FontAwesome.IMAGE);
        showB.setIcon(FontAwesome.IMAGE);
        showB.setEnabled(false);
        previousButton = new Button("Wstecz");
        previousButton.setIcon(FontAwesome.ARROW_CIRCLE_O_LEFT);
    }

    public static double getMaxValueAndSetSize() {
        double maxValue = 1000;
        List<SectorDb> sectorsFromDba = SectorDao.readAllByResearchId(1);


        return maxValue;
    }

    private static List<VariableDb> getAllVariables(List<SectorDb> sectorsFromDba) {
        List<VariableDb> allVariables = new ArrayList<>();

        for (SectorDb sectorDb : sectorsFromDba) {
            allVariables.addAll(VariableDao.readAllStateVariablesBySectorId(sectorDb.getId()));
        }
        return allVariables;
    }
}
