/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views.optimalisation;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.TimeSeriesDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.TimeSeriesDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.ValuesForecaster;
import com.agh.ekonometria.views.optimalisation.matrixes.XtPlusOneMatrix;
import com.agh.ekonometria.views.optimalisation.matrixes.YtPlusOneMatrix;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import java.util.ArrayList;
import java.util.List;

public class ForecastLayout extends VerticalLayout {
    int researchId;
    private List<VariableDb> allVariables;

    public ForecastLayout(int researchId) {
        this.researchId = researchId;
        setSpacing(true);
        addComponent(new VerticalLayout(new Label("Macierz x(t+1)"),new XtPlusOneMatrix(generate_x_t_plus_1(),allVariables)));
        addComponent(new VerticalLayout(new Label("Macierz y(t+1)"),new YtPlusOneMatrix(generate_y_t_plus_1(),researchId)));

    }

    private double[][] generate_x_t_plus_1() {
        List<SectorDb> sectorsFromDba =  SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        allVariables = getAllVariables(sectorsFromDba);
        int size = allVariables.size();
        int howManyYearsInFuture = 5;
        Double[][] matrixAValues = new Double[size][howManyYearsInFuture];

        for (int i = 0; i < size; i++) {
            List<TimeSeriesDb> timeSeries = TimeSeriesDao.readAllByVariableId(allVariables.get(i).getId());
            Double[] values = new Double[timeSeries.size()];
            for (int j = 0; j < timeSeries.size(); j++) {
                Double val = timeSeries.get(j).getValue();
                if (val == null) {
                    val = 0.0;
                }
                values[j] = val;
            }

            matrixAValues[i] = ValuesForecaster.forecastValues(values, howManyYearsInFuture);
        }
        double[][] ret = new double[matrixAValues[0].length][matrixAValues.length];
        for (int i = 0; i < matrixAValues.length; i++)
            for (int j = 0; j < matrixAValues[0].length; j++)
                ret[j][i] = matrixAValues[i][j];
        return ret;
    }

    private double[][] generate_y_t_plus_1() {
        List<SectorDb> sectorsFromDba = SectorDao.readAllByResearchId(Integer.valueOf(researchId));
        List<VariableDb> allVariables = getOutputVariables(sectorsFromDba);
        int size = allVariables.size();
        int howManyYearsInFuture = 5;
        Double[][] matrixAValues = new Double[size][howManyYearsInFuture];

        for (int i = 0; i < size; i++) {
            List<TimeSeriesDb> timeSeries = TimeSeriesDao.readAllByVariableId(allVariables.get(i).getId());
            Double[] values = new Double[timeSeries.size()];
            for (int j = 0; j < timeSeries.size(); j++) {
                Double val = timeSeries.get(j).getValue();
                if (val == null) {
                    val = 0.0;
                }
                values[j] = val;
            }

            matrixAValues[i] = ValuesForecaster.forecastValues(values, howManyYearsInFuture);
        }
        double[][] ret = new double[matrixAValues[0].length][matrixAValues.length];
        for (int i = 0; i < matrixAValues.length; i++)
            for (int j = 0; j < matrixAValues[0].length; j++)
                ret[j][i] = matrixAValues[i][j];
        return ret;
    }

    private List<VariableDb> getOutputVariables(List<SectorDb> sectorsFromDba) {
        List<VariableDb> allVariables = new ArrayList<>();

        for (SectorDb sectorDb : sectorsFromDba) {
            allVariables.addAll(VariableDao.readAllTypeVariablesBySectorId(sectorDb.getId(), VariableDb.OUTPUT_VARIABLE));
        }
        return allVariables;
    }

    private static List<VariableDb> getAllVariables(List<SectorDb> sectorsFromDba) {
        List<VariableDb> allVariables = new ArrayList<>();

        for (SectorDb sectorDb : sectorsFromDba) {
            allVariables.addAll(VariableDao.readAllStateVariablesBySectorId(sectorDb.getId()));
        }
        return allVariables;
    }
}
