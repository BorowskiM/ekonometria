/* Copyright 2016 Sabre Holdings */
package com.agh.ekonometria.views;

public enum VariableType {
    STATE("Stanu",0), INPUT("Wejściowa",1), OUTPUT("Wyjściowa",2);

    private String name;
    private Integer number;

    VariableType(String name, Integer number)
    {
        this.name = name;
        this.number = number;
    }

    public String getName()
    {
        return name;
    }
    public Integer getNumber()
    {
        return number;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
