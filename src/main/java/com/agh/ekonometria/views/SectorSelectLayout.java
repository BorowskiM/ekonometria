package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.model.Sector;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Artur on 2015-06-07.
 */
public class SectorSelectLayout extends VerticalLayout {
    private ArrayList<Sector> sectorData = new ArrayList<>();
    private HashMap<Button, Sector> buttonSectorMap = new HashMap<>();
    private ArrayList<Button> buttons = new ArrayList<>();
    private ArrayList<Button> clickedButtons = new ArrayList<>();
    private int activeButtons = 0;
    private Panel panelWithModels;
    private HorizontalLayout sectorButtons;
    private SectorAddWindow window = new SectorAddWindow();
    private String researchId;
    private TableLayout tableLayout;

    public SectorSelectLayout(String researchId, TableLayout tableLayout) {
        this.researchId = researchId;
        this.tableLayout = tableLayout;
        init();
        initComponents();
        addComponents();

    }

    private void init() {
        setMargin(true);
    }

    private void initComponents() {
        // Adding new items
        addPredefinedSectors();
        generateRoundButtons();
    }

    public ArrayList<Button> getClickedButtons() {
        return clickedButtons;
    }

    public HashMap<Button, Sector> getButtonSectorMap() {
        return buttonSectorMap;
    }

    private void generateRoundButtons() {
        for (Button but : buttons) {
            but.setStyleName("roundButtonStyle");
            but.addClickListener(event -> changeStyle(but));
        }
    }

    private void addPredefinedSectors() {
        List<SectorDb> sectors = SectorDao.readAllByResearchId(Integer.valueOf(researchId));

        for (SectorDb sectorDb : sectors) {
            sectorData.add(new Sector().withId(sectorDb.getId()).withName(sectorDb.getName()).withShortcutName(sectorDb.getAbbreviation()).withDescription(sectorDb.getDescription()));
        }
        addSectorsToButtons();
    }

    private void addSectorsToButtons() {
        for (Sector sec : sectorData) {
            Button b = new Button();
            b.setCaption(sec.getAbbreviation());
            b.setDescription(sec.getName());
            b.setData(sec.getId());
            buttons.add(b);
            buttonSectorMap.put(b, sec);
        }
    }

    private void addNewSector() {
        //TODO przy naciśnięciu CANCEL lub X ma nie dodawać pustego sektora
        Button newSector = new Button();
        UI.getCurrent().addWindow(window);
        window.setButtonWhichNameShouldBeUpdated(newSector);
        newSector.setStyleName("roundButtonStyle");
        newSector.addClickListener(event -> changeStyle(newSector));
        buttons.add(newSector);
        sectorData.add(window.getSectorToEdit());
        buttonSectorMap.put(newSector, window.getSectorToEdit());
        refreshSectorButtons();
    }

    private void deleteSelectedSector() {
        if (clickedButtons.size() == 1) {
            buttons.remove(clickedButtons.get(0));
            sectorData.remove(buttonSectorMap.get(clickedButtons.get(0)));
            buttonSectorMap.remove(clickedButtons.get(0));
            clickedButtons.clear();
            refreshSectorButtons();
        }
    }

    private void refreshSectorButtons() {
        removeComponent(sectorButtons);
        sectorButtons = new HorizontalLayout(buttons.toArray(new Button[buttons.size()]));
        addComponent(sectorButtons);
    }

    private void modifySelectedSector() {
        if (clickedButtons.size() == 1) {
            //TODO przy naciśnięciu CANCEL lub X ma nie dodawać pustego sektora
            Button sectorToModify = clickedButtons.get(0);
            window.setSectorToEdit(buttonSectorMap.get(sectorToModify));
            UI.getCurrent().addWindow(window);
            window.setButtonWhichNameShouldBeUpdated(sectorToModify);
//            sectorToModify.setStyleName("roundButtonStyle");
//            sectorToModify.addClickListener(event -> changeStyle(newSector));
//            buttons.add(newSector);
//            sectorData.add(window.getSectorToEdit());
//            buttonSectorMap.put(newSector, window.getSectorToEdit());
            refreshSectorButtons();
        }
    }

    private void changeStyle(Button b) {
        if (clickedButtons.size() < 2) {
            if (b.getStyleName().equals("roundButtonStyle")) {
                b.setStyleName("roundButtonStyleClicked");
                clickedButtons.add(b);
            } else if (b.getStyleName().equals("roundButtonStyleClicked")) {
                b.setStyleName("roundButtonStyle");
                clickedButtons.remove(b);
            }
        } else {
            if (b.getStyleName().equals("roundButtonStyle")) {
                b.setStyleName("roundButtonStyleClicked");
                clickedButtons.add(b);
                clickedButtons.get(0).setStyleName("roundButtonStyle");
                clickedButtons.remove(0);
            } else if (b.getStyleName().equals("roundButtonStyleClicked")) {
                b.setStyleName("roundButtonStyle");
                clickedButtons.remove(b);
            }
        }

        if (clickedButtons.size() == 2) {
            Integer idSec1 = Integer.valueOf(clickedButtons.get(0).getData().toString());
            Integer idSec2 = Integer.valueOf(clickedButtons.get(1).getData().toString());

            tableLayout.generateTableAfterClick(idSec1, idSec2);
        }
    }

    private void addComponents() {
        sectorButtons = new HorizontalLayout(buttons.toArray(new Button[buttons.size()]));
        addComponent(sectorButtons);
    }
}
