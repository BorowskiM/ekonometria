/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.buttons;

import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Sector;
import com.agh.ekonometria.views.AddVariableToSectorWindow;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;

public class AddVariableButton
        extends Button
{
    private AddVariableToSectorWindow window = new AddVariableToSectorWindow();
    private Object sectorId;
    private Sector sector;
    private TreeTable treeTable;

    private int sectorIdDb;

    public AddVariableButton()
    {
        this.setIcon(FontAwesome.PLUS);
        addCloseWindowListener();
        this.addClickListener(event -> UI.getCurrent().addWindow(window));
    }

    private void addCloseWindowListener()
    {
        window.addCloseListener(e -> {
            RemoveVariableButton removeVariableButton = new RemoveVariableButton();
            HorizontalLayout horizontalLayoutWithButton = new HorizontalLayout(removeVariableButton);
            horizontalLayoutWithButton.setSizeFull();
            horizontalLayoutWithButton.setComponentAlignment(removeVariableButton, Alignment.MIDDLE_CENTER);

            final Object variableId = treeTable.addItem(new Object[] { window.getVariableName(), horizontalLayoutWithButton }, null);
            treeTable.setParent(variableId, sectorId);
            treeTable.setChildrenAllowed(variableId, false);
            Integer variableIdDb = VariableDao.createWithType(window.getVariableName(), sectorIdDb,window.getVariableTypeNumber());
            VariableDb variableDb = new VariableDb(variableIdDb, window.getVariableName(), sectorIdDb, window.getVariableTypeNumber());
            removeVariableButton
                    .withSector(sector)
                    .withVariableIdDb(variableDb.getId())
                    .withTreeTable(treeTable)
                    .withVariableIdInTable(variableId)
                    .withVariable(variableDb);

            sector.withVariables(variableDb);
        });
    }

    public void setSectorIdInTable(Object sectorId)
    {
        this.sectorId = sectorId;
    }

    public void setTreeTable(TreeTable treeTable)
    {
        this.treeTable = treeTable;
    }

    public void setSector(Sector sector)
    {
        this.sector = sector;
    }

    public void setSectorIdDb(int sectorIdDb)
    {
        this.sectorIdDb = sectorIdDb;
    }
}
