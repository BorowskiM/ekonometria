/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.buttons;

import com.vaadin.ui.Button;

public class CustomToResearchLayoutButton extends Button
{
    private int researchId;

    public CustomToResearchLayoutButton()
    {

    }

    public CustomToResearchLayoutButton withResearchId(int researchId)
    {
        this.researchId = researchId;
        return this;
    }
}
