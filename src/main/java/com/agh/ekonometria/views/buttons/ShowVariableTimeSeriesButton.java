/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.buttons;

import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Sector;
import com.agh.ekonometria.views.VariableShowTimeSeries;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;

public class ShowVariableTimeSeriesButton
        extends Button
{
    private Object variableId;
    private Sector sector;
    private VariableDb variable;
    private TreeTable treeTable;
    private int variableIdDb;


    private int sectorIdDb;

    public ShowVariableTimeSeriesButton()
    {
        this.setIcon(FontAwesome.CALENDAR);
        this.addClickListener(event -> {
            UI.getCurrent().addWindow(new VariableShowTimeSeries(variable, variableIdDb));
        });
    }

    public ShowVariableTimeSeriesButton withVariableIdInTable(Object variableId)
    {
        this.variableId = variableId;
        return this;
    }

    public ShowVariableTimeSeriesButton withTreeTable(TreeTable treeTable)
    {
        this.treeTable = treeTable;
        return this;
    }

    public ShowVariableTimeSeriesButton withSector(Sector sector)
    {
        this.sector = sector;
        return this;
    }

    public ShowVariableTimeSeriesButton withVariableIdDb(int variableIdDb)
    {
        this.variableIdDb = variableIdDb;
        return this;
    }
    public ShowVariableTimeSeriesButton withVariable(VariableDb variable)
    {
        this.variable = variable;
        return this;
    }
}
