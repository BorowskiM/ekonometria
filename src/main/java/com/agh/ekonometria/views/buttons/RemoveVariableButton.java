/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.buttons;

import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Sector;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.TreeTable;
import java.util.List;

public class RemoveVariableButton
        extends Button
{
    private Object variableId;
    private Sector sector;
    private VariableDb variable;
    private TreeTable treeTable;
    private int variableIdDb;

    public RemoveVariableButton()
    {
        this.setIcon(FontAwesome.MINUS_CIRCLE);
        addListener();
    }

    private void addListener()
    {
        this.addClickListener(event -> {
            treeTable.removeItem(variableId);
            VariableDao.delete(variableIdDb);
            sector.getVariables().remove(variable);
        });
    }

    public RemoveVariableButton withVariableIdInTable(Object variableId)
    {
        this.variableId = variableId;
        return this;
    }

    public RemoveVariableButton withTreeTable(TreeTable treeTable)
    {
        this.treeTable = treeTable;
        return this;
    }

    public RemoveVariableButton withSector(Sector sector)
    {
        this.sector = sector;
        return this;
    }

    public RemoveVariableButton withVariableIdDb(int variableIdDb)
    {
        this.variableIdDb = variableIdDb;
        return this;
    }
    public RemoveVariableButton withVariable(VariableDb variable)
    {
        this.variable = variable;
        return this;
    }
}
