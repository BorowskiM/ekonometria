/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views.buttons;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Sector;
import com.agh.ekonometria.model.Variable;
import com.agh.ekonometria.views.AddVariableToSectorWindow;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class RemoveSectorButton
        extends Button
{
    private Object sectorId;
    private Sector sector;
    private List<Sector> sectors;
    private TreeTable treeTable;
    private int sectorIdDb;

    public RemoveSectorButton()
    {
        this.setIcon(FontAwesome.MINUS);
        addListener();
    }

    private void addListener()
    {
        this.addClickListener(event -> {
            Collection<?> children = new LinkedList<Object>(treeTable.getChildren(sectorId));
            if (children != null)
            {
                for(Object id: children)
                {
                    treeTable.removeItem(id);
                }
            }
            treeTable.removeItem(sectorId);
            for(VariableDb variable: sector.getVariables())
            {
                VariableDao.delete(variable.getId());
            }
            SectorDao.delete(sectorIdDb);
            sectors.remove(sector);
        });
    }

    public RemoveSectorButton withSectorIdInTable(Object sectorId)
    {
        this.sectorId = sectorId;
        return this;
    }

    public RemoveSectorButton withTreeTable(TreeTable treeTable)
    {
        this.treeTable = treeTable;
        return this;
    }

    public RemoveSectorButton withSector(Sector sector)
    {
        this.sector = sector;
        return this;
    }

    public RemoveSectorButton withSectors(List<Sector> sectors)
    {
        this.sectors = sectors;
        return this;
    }

    public RemoveSectorButton withSectorIdDb(int sectorIdDb)
    {
        this.sectorIdDb = sectorIdDb;
        return this;
    }
}
