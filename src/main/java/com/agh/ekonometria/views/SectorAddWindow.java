/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.model.Sector;
import com.vaadin.ui.Button;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SectorAddWindow extends Window
{
    private TextField name = new TextField();
    private TextField shortcut = new TextField();
    private RichTextArea description = new RichTextArea();
    private Button zapiszButton = new Button("Zapisz");

    private Sector sectorToEdit = new Sector();
    private VerticalLayout windowLayout = new VerticalLayout();

    private Button buttonWhichDescriptionShouldBeUpdated;

    public SectorAddWindow()
    {
        setStyleName("upload-info");
        setDraggable(true);
        initComponents();
        addListeners();
        fillLayout();
        setContent(windowLayout);
    }

    private void fillLayout()
    {
        windowLayout.addComponent(name);
        windowLayout.addComponent(shortcut);
        windowLayout.addComponent(description);
        windowLayout.addComponent(zapiszButton);
    }

    private void addListeners()
    {
        zapiszButton.addClickListener(event -> {
            if (buttonWhichDescriptionShouldBeUpdated != null)
            {
                buttonWhichDescriptionShouldBeUpdated.setCaption(shortcut.getValue());
                buttonWhichDescriptionShouldBeUpdated.setDescription(getName());
            }
            close();

        });
    }

    public String getName()
    {
        return name.getValue();
    }

    public String getShortcutName()
    {
        return shortcut.getValue();
    }

    public String getDescription()
    {
        return description.getValue();
    }

    private void initComponents()
    {
        name.setCaption("Podaj nazwę sektora:");
        shortcut.setCaption("Podaj skróconą nazwę sektora:");
        description.setCaption("Podaj opis sektora:");
    }

    public void setButtonWhichNameShouldBeUpdated(Button buttonWhichDescriptionShouldBeUpdated)
    {
        this.buttonWhichDescriptionShouldBeUpdated = buttonWhichDescriptionShouldBeUpdated;
    }

    public Sector getSectorToEdit()
    {
        return sectorToEdit;
    }

    public void setSectorToEdit(Sector sectorToEdit)
    {
        this.sectorToEdit = sectorToEdit;
        name.setValue(sectorToEdit.getName());
        shortcut.setValue(sectorToEdit.getAbbreviation());
        description.setValue(sectorToEdit.getDescription());

    }
}
