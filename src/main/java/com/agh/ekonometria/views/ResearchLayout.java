/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.ResearchDao;
import com.agh.ekonometria.dao.model.ResearchDb;
import com.agh.ekonometria.utils.NotificationProducer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import static com.agh.ekonometria.utils.NotificationProducer.showInformationNotification;

public class ResearchLayout extends VerticalLayout
{

    private HorizontalLayout editLayout;
    private TextField modelNameTextField;
    private Button addButton;
    private Button nextButton;
    private Table table;
    private int numberOfItems = 1;
    private Button removeButton;
    private Button modificationButton;
    private Button saveButton;

    public ResearchLayout()
    {
        init();
        initComponents();
        addAllComponents();
    }

    private void init()
    {
        setSpacing(true);
    }

    private void initComponents()
    {
        initTable();
        editLayout = new HorizontalLayout();
        editLayout.setSpacing(true);
        editLayout.addComponent(new Label("Dodaj badanie:"));
        modelNameTextField = new TextField();
        addButton = new Button();
        addButton.setIcon(FontAwesome.PLUS_SQUARE_O);
        nextButton = new Button("Dalej");
        nextButton.setIcon(FontAwesome.ARROW_CIRCLE_O_RIGHT);
        editLayout.addComponent(modelNameTextField);
        editLayout.addComponent(addButton);
        nextButton.setEnabled(false);
        addButton.addClickListener(event -> {
            int researchId = ResearchDao.create(modelNameTextField.getValue(), getUser());
            Button removeButton = createRemoveButton();
            Button modificationButton = createModificationButton();
            Button saveButton = createSaveButton();
            Object rowId = table.addItem(new Object[] { String.valueOf(researchId), modelNameTextField.getValue(), getUser(), LocalDateTime.now().toString(), new HorizontalLayout(removeButton, modificationButton, saveButton) }, null);
            addListeners(researchId,removeButton,modificationButton,saveButton,rowId);
            table.setPageLength(table.size());
            removeComponent(table);
            removeComponent(nextButton);
            addComponent(table);
            addComponent(nextButton);
        });
        nextButton.addClickListener(event ->
        {
            removeAllComponents();
            addComponent(new ResearchDetailsLayout((String)table.getContainerProperty(table.getValue(),"Id").getValue()));

        });

    }

    private String getUser()
    {
        return ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }

    private void initTable()
    {
        table = new Table("Dostępne badania: ");
        table.setSelectable(true);
        table.addContainerProperty("Id", String.class, null);
        table.addContainerProperty("Nazwa", String.class, null);
        table.addContainerProperty("Autor",String.class,null);
        table.addContainerProperty("Data", String.class, null);
        table.addContainerProperty("", HorizontalLayout.class, null);
        List<ResearchDb> researchDbs = ResearchDao.readAll();
        for(ResearchDb r: researchDbs)
        {

            Button removeButton = createRemoveButton();
            Button modificationButton = createModificationButton();
            Button saveButton = createSaveButton();
            Object rowId = table.addItem(new Object[] { r.getId().toString(),r.getName(), r.getCreatedBy(), r.getCreatedAt().toString(), new HorizontalLayout(removeButton,modificationButton,saveButton) }, numberOfItems++);
            addListeners(r.getId(), removeButton, modificationButton, saveButton, rowId);
        }
        table.setPageLength(table.size());
        table.addItemClickListener(event -> nextButton.setEnabled(true));
    }

    private void addListeners(final int researchId, Button removeButton, Button modificationButton, final Button saveButton, final Object rowId)
    {
        removeButton.addClickListener(event -> {
            ResearchDao.delete(researchId);
            table.removeItem(rowId);
            table.setPageLength(table.size());
            removeComponent(table);
            addComponent(table);
        });
        modificationButton.addClickListener(event -> {
            table.setEditable(true);
            saveButton.setEnabled(true);
        });
        saveButton.addClickListener(event -> {
            ResearchDao.delete(researchId);
            ResearchDao.create(table.getItem(rowId).getItemProperty("Name").toString(),table.getItem(rowId).getItemProperty("Author").toString());
            table.setEditable(false);
            saveButton.setEnabled(false);
            showInformationNotification("Dane zostaly pomyslnie zapisane");
        });
    }

    private Button createSaveButton()
    {
        Button saveButton = new Button();
        saveButton.setIcon(FontAwesome.SAVE);
        saveButton.setEnabled(false);
        return saveButton;
    }

    private Button createModificationButton()
    {
        Button modificationButton = new Button();
        modificationButton.setIcon(FontAwesome.EDIT);
        return modificationButton;
    }

    private Button createRemoveButton()
    {
        Button removeButton = new Button();
        removeButton.setIcon(FontAwesome.MINUS);
        return removeButton;
    }

    private void addAllComponents()
    {
        addComponent(editLayout);
        addComponent(table);
        addComponent(nextButton);
    }
}
