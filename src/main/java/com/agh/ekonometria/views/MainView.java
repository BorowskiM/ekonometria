package com.agh.ekonometria.views;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import java.util.ArrayList;
import java.util.List;
import com.vaadin.ui.VerticalLayout;
import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;


@Component
@Scope("prototype")
@VaadinView(MainView.NAME)
@Theme("myTheme")
public class MainView extends Panel implements View
{
    public static final String NAME = "";
    private VerticalLayout layout;

    @PostConstruct
    public void PostConstruct()
    {
        setSizeFull();
        initLayout();
        setContent(layout);
    }

    private void initLayout()
    {
        layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.setMargin(true);

        layout.addComponent(new ResearchLayout());
        layout.addComponent(new Link("Wyloguj", new ExternalResource("/j_spring_security_logout")));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event)
    {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = new ArrayList<String>();
        for (GrantedAuthority grantedAuthority : user.getAuthorities())
        {
            roles.add(grantedAuthority.getAuthority());
        }
    }
}
