/**
 * Created by Artur on 2015-09-20.
 */
package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.TimeSeriesTable;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class VariableShowTimeSeries extends Window {
    private VariableDb variableDb;
    private int variableIdDb;
    private TextField name = new TextField();
    private TimeSeriesTable table;
    private Button zamknijButton = new Button("Zamknij");
    private VerticalLayout windowLayout = new VerticalLayout();

    public VariableShowTimeSeries(VariableDb variable, int variableIdDb) {
        this.variableDb = variable;
        this.variableIdDb = variableIdDb;
        table = new TimeSeriesTable(variableIdDb);
        setStyleName("upload-info");
        setDraggable(true);
        setResizable(true);
        setWidth("1275px");
        setHeight("300px");
        initComponents();
        addListeners();
        fillLayout();
        setContent(windowLayout);
        windowLayout.setSpacing(true);
    }

    private void fillLayout() {
        windowLayout.addComponent(name);
        windowLayout.addComponent(table);
        windowLayout.addComponent(zamknijButton);
    }

    private void addListeners() {
        zamknijButton.addClickListener(event -> {
            close();
        });
    }


    private void initComponents() {

        if (variableDb != null) {
            name.setCaptionAsHtml(true);
            name.setCaption("<b>Szereg czasowy</b></br></br>");
            name.setValue(variableDb.getName());
            name.setHeight("80px");
            name.setWidth("1275px");
            name.setReadOnly(true);
        }
    }

    public VariableDb getVariableDb() {
        return variableDb;
    }

    public void setVariableDb(VariableDb variableDb) {
        this.variableDb = variableDb;
    }

    public int getVariableIdDb() {
        return variableIdDb;
    }

    public void setVariableIdDb(int variableIdDb) {
        this.variableIdDb = variableIdDb;
    }
}
