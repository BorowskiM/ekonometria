/**
 * Created by Artur on 2015-09-20.
 */
package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.TimeSeriesTable;
import com.vaadin.ui.*;

public class VariableShowTimeSeriesVerticalLayout extends VerticalLayout {
    private VariableDb variableDb;
    private int variableIdDb;
    private TimeSeriesTable table;
    private VerticalLayout windowLayout = new VerticalLayout();

    public VariableShowTimeSeriesVerticalLayout(VariableDb variable, int variableIdDb) {
        this.variableDb = variable;
        this.variableIdDb = variableIdDb;
        table = new TimeSeriesTable(variableIdDb);
        initComponents();
        addComponent(table);
    }


    private void initComponents() {

        if (variableDb != null) {
            table.setCaption(variableDb.getName());



        }
    }

    public VariableDb getVariableDb() {
        return variableDb;
    }

    public void setVariableDb(VariableDb variableDb) {
        this.variableDb = variableDb;
    }

    public int getVariableIdDb() {
        return variableIdDb;
    }

    public void setVariableIdDb(int variableIdDb) {
        this.variableIdDb = variableIdDb;
    }
}
