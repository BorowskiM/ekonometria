/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import static com.vaadin.ui.Table.COLUMN_HEADER_MODE_HIDDEN;

import com.agh.ekonometria.SpringSecurityHelper;
import com.agh.ekonometria.dao.ValueDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.ValueDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Converter;
import com.agh.ekonometria.model.DataHolder;
import com.agh.ekonometria.model.TableDataFiller;
import com.agh.ekonometria.model.VariableKey;
import com.agh.ekonometria.utils.NotificationProducer;
import com.agh.ekonometria.views.tablecell.HeaderVariableTextField;
import com.agh.ekonometria.views.tablecell.MyNativeSelect;
import com.agh.ekonometria.views.tablecell.ScaleTypeOptionGroup;
import com.agh.ekonometria.views.tablecell.SingleCellLayout;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class TableLayout extends VerticalLayout
{
    private Map<MultiKey, DataHolder> data = new LinkedHashMap<>();

    private Table table = new Table();
    private OptionGroup informationType;
    private Boolean modifyHeaders = true;
    private Integer numberOfVariableAdded = 0;
    private Panel panelWithTable;
    private TableDataFiller tableDataFiller = new TableDataFiller();
    private boolean isQualityData;
    private String researchID;
    private boolean rowHeader;
    private Integer actualFirstSectorId;
    private Integer actualSecondSectorId;

    public TableLayout(String researchID)
    {
        this.researchID = researchID;
        init();
        initComponents();
        addComponents();
    }

    private void init()
    {
        table.setEditable(true);
    }

    private void initComponents()
    {
        String basepath = VaadinService.getCurrent()
                .getBaseDirectory().getAbsolutePath();

        // Image as a file resource
        FileResource resource = new FileResource(new File(basepath +
                "/WEB-INF/images/plus.png"));
        Embedded reindeerImage = new Embedded(null, new ThemeResource("images/reindeer.png"));
        reindeerImage.setWidth("240px");
        reindeerImage.setHeight("180px");
        // Adding new items
        initInformationType();
        if (!SpringSecurityHelper.hasRole("ROLE_ADMIN"))
        {
            modifyHeaders = false;

        }
        createRowsAndColumns(3, 4, null, null);
    }

    private void initInformationType()
    {
        informationType = new OptionGroup();
        informationType.setImmediate(true);
        informationType.addItems("Dane ilościowe", "Dane jakościowe");
        informationType.addStyleName("horizontal");
        informationType.setValue(String.valueOf(informationType.getItemIds().iterator().next()));
        informationType.addValueChangeListener(event -> {
            informationType.getConvertedValue();
            Map<MultiKey, DataHolder> dataBeforeMap = new Converter().retrieveDataFromTable(table);
            removeComponent(panelWithTable);
            table = new Table();
            panelWithTable = new Panel(table);
            addComponents(panelWithTable);
            List<VariableDb> variables1 = VariableDao.readAllStateVariablesBySectorId(actualFirstSectorId);
            List<VariableDb> variables2 = VariableDao.readAllStateVariablesBySectorId(actualSecondSectorId);
            if (informationType.isSelected("Dane jakościowe"))
            {
                isQualityData = true;
            }
                else
                {
                    isQualityData = false;

                }
                createRowsAndColumns(variables1.size(), variables2.size(), variables1, variables2);
            });
    }

    private void showNotification(String message)
    {
        Notification notification = new Notification(message, "", Notification.Type.HUMANIZED_MESSAGE, true);
        notification.show(Page.getCurrent());
        notification.setDelayMsec(3000);
        notification.setPosition(Position.TOP_RIGHT);
    }

    public void saveData()
    {
        numberOfVariableAdded = 0;
        table.commit();

        data = new Converter().retrieveDataFromTable(table);
        for(MultiKey multiKey: data.keySet())
        {
            Integer firstVariableId = ((VariableKey) multiKey.getKey(0)).getVariableId();
            Integer  secondVariableId = ((VariableKey) multiKey.getKey(1)).getVariableId();
            if(SpringSecurityHelper.hasRole("ROLE_ADMIN") || SpringSecurityHelper.hasRole("ROLE_MODERATOR"))
            {
                VariableDb variableDb1 = VariableDao.read(firstVariableId);
                VariableDb variableDb2 = VariableDao.read(secondVariableId);
                variableDb1.setName(((VariableKey) multiKey.getKey(0)).getVariableName());
                variableDb2.setName(((VariableKey) multiKey.getKey(1)).getVariableName());
                VariableDao.update(variableDb1);
                VariableDao.update(variableDb2);
            }
            ValueDb valueDb = null;
            if(firstVariableId != null && secondVariableId != null)
            {
                valueDb = ValueDao.readByVariableIds(firstVariableId, secondVariableId);
            }
            if(valueDb != null)
            {
                ValueDao.delete(valueDb.getId());
            }
            DataHolder dh = data.get(multiKey);
            if(firstVariableId!= null && secondVariableId != null)
            {
                ValueDao.create(firstVariableId, secondVariableId, String.valueOf(dh.getValue()), dh.isQualityData(), getUser(), dh.getNote().getValue(), null, dh.getSelectTypeOfScale());
            }

        }

        showNotification("Dane zostały zapisane");
    }
    private String getUser()
    {
        return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }

    private void createRowsAndColumns(int sizeWidth, int sizeHeight, List<VariableDb> variables1, List<VariableDb> variables2)
    {
        for (int i = 0; i < sizeHeight; i++)
        {
            String propertyID = String.valueOf(i);
            table.addContainerProperty(propertyID, HorizontalLayout.class, null);
        }

        for (int i = 0; i < sizeWidth; i++)
        {
            Object[] values = new Object[sizeHeight];
            fillValuesArray(sizeHeight, i, values, variables1, variables2);
            table.addItem(values, i);
        }

        tuneTable();

        for (int i = 0; i < sizeWidth; i++)
        {
            for (int j = 0; j < sizeHeight; j++)
            {
                setStyleForVariableHeader(i, j);
                setVisibility(i, j);
                setModificationAllowance(i, j);
            }
        }
    }

    private void setModificationAllowance(int i, int j)
    {
        if (!modifyHeaders)
        {
            if (isHeader(i, j))
            {
                ((HorizontalLayout) table.getItem(i).getItemProperty(String.valueOf(j)).getValue()).getComponent(0).setReadOnly(true);
            }
        }
    }

    private void setVisibility(int i, int j)
    {
        if (i == 0 && j == 0)
        {
            // ((Field) table.getItem(String.valueOf(i)).getItemProperty(String.valueOf(j))).setValue(null);
            ((HorizontalLayout) table.getItem(i).getItemProperty(String.valueOf(j)).getValue()).setVisible(false);
        }
    }

    private void setStyleForVariableHeader(int i, int j)
    {
        if (isHeader(i, j))
        {
            ((HorizontalLayout) table.getItem(i).getItemProperty(String.valueOf(j)).getValue()).getComponent(0).setStyleName("fieldHeaderStyle");
        }
    }

    private void tuneTable()
    {
        table.setColumnHeaderMode(COLUMN_HEADER_MODE_HIDDEN);
        table.setPageLength(table.size());

        // Allow selecting items from the table.
        table.setSelectable(true);

        // Send changes in selection immediately to server.
        table.setImmediate(true);

        final Label current = new Label("Selected: -");

        table.addValueChangeListener(event -> current.setValue("Selected: " + table.getValue()));

        table.addColumnResizeListener(event -> {
            // Get the new width of the resized column
            int width = event.getCurrentWidth();

            // Get the property ID of the resized column
            String column = (String) event.getPropertyId();

            // Do something with the information
            table.setColumnFooter(column, String.valueOf(width) + "px");
        });

        table.setCellStyleGenerator((source, itemId, propertyId) -> {
            // Row style setting, not relevant in this example.
            if (propertyId == null)
                return "green"; // Will not actually be visible

            int row = ((Integer) itemId).intValue();
            int col = Integer.parseInt((String) propertyId);

            // The first column.
            if (isHeader(col, row))
                return "rowheader";
                else
                    return "white";

            });

        table.setEditable(true);
    }

    private void fillValuesArray(int n, int i, Object[] values, List<VariableDb> variables1, List<VariableDb> variables2)
    {
        for (int j = 0; j < n; j++)
        {

            HorizontalLayout horizontalLayout = new HorizontalLayout();
            if (isHeader(i, j))
            {
                initAndFillHeaders(i, variables1, variables2, j, horizontalLayout);
            }
            else
            {
                horizontalLayout.setSpacing(true);
                TextField quantityTextField = new TextField();
                MyNativeSelect myNativeSelect = new MyNativeSelect();
                ScaleTypeOptionGroup scaleTypeOptionGroup = new ScaleTypeOptionGroup("Pięciostopniowa", myNativeSelect);
                ValueDb valueDb = null;
                if (variables1 != null && variables2 != null /*&& variables1.size() >= i && variables2.size() >= j*/)
                {
                    valueDb = ValueDao.readByVariableIds(variables1.get(i).getId(), variables2.get(j).getId());
                }
                if (valueDb != null)
                {
                    if (valueDb.isQualityData())
                    {
                        myNativeSelect.setValue(Double.valueOf(valueDb.getValue()));
                        scaleTypeOptionGroup.setValue(scaleTypeOptionGroup.getStringTypeOfScale(valueDb.getScaleId()));
                    }
                    else
                    {
                        quantityTextField.setValue(valueDb.getValue());
                    }
                }
                SingleCellLayout singleCellLayout = new SingleCellLayout().withTextField(quantityTextField).withMyNativeSelect(myNativeSelect).withScaleTypeOptionGroup(scaleTypeOptionGroup);
                if (valueDb != null && !valueDb.getValue().equals("") && !valueDb.getValue().equals("0.0"))
                {
                    singleCellLayout.init(valueDb.isQualityData());
                }
                else
                {
                    singleCellLayout.init(isQualityData);
                }
                horizontalLayout.addComponent(singleCellLayout);
            }
            values[j] = horizontalLayout;
        }
    }

    private void initAndFillHeaders(int i, List<VariableDb> variables1, List<VariableDb> variables2, int j, HorizontalLayout horizontalLayout)
    {
        HeaderVariableTextField tf = new HeaderVariableTextField();
        if (isRowHeader(j) && variables1 != null && variables1.size() > i)
        {
            String name = variables1.get(i).getName();
            tf.setValue(name);
            tf.setVariableId(variables1.get(i).getId());
            tf.setDescription(name);
        }
        else if (variables2 != null && variables2.size() > j)
        {
            String name = variables2.get(j).getName();
            tf.setValue(name);
            tf.setVariableId(variables2.get(j).getId());
            tf.setDescription(name);

        }
        horizontalLayout.addComponent(tf);
        horizontalLayout.setComponentAlignment(tf, Alignment.MIDDLE_CENTER);
    }

    private boolean isHeader(int i, int j)
    {
        return i == 0 || j == 0;
    }

    private void addRowAndColumn()
    {
        numberOfVariableAdded++;
        Map<MultiKey, DataHolder> dataBeforeMap = new Converter().retrieveDataFromTable(table);
        if(actualFirstSectorId == null || actualSecondSectorId == null)
        {
            NotificationProducer.showInformationNotification("You have to select two sectors");
            return;
        }
        removeComponent(panelWithTable);
        List<VariableDb> variables1 = VariableDao.readAllStateVariablesBySectorId(actualFirstSectorId);
        List<VariableDb> variables2 = VariableDao.readAllStateVariablesBySectorId(actualSecondSectorId);
        table = new Table();
        panelWithTable = new Panel(table);
        int size = (variables1.size() < variables2.size()) ? variables1.size() : variables2.size();
        createRowsAndColumns(variables1.size(), size+numberOfVariableAdded, null, null);
        tableDataFiller.fillTableWithProperData(dataBeforeMap, table, size + numberOfVariableAdded);
        addComponents(panelWithTable);
    }

    public void generateTableAfterClick(Integer idSec1, Integer idSec2)
    {
        this.actualFirstSectorId = idSec1;
        this.actualSecondSectorId = idSec2;

        List<VariableDb> variables1 = VariableDao.readAllStateVariablesBySectorId(idSec1);
        List<VariableDb> variables2 = VariableDao.readAllStateVariablesBySectorId(idSec2);
        int size = (variables1.size() < variables2.size()) ? variables1.size() : variables2.size();
        removeComponent(panelWithTable);
        table = new Table();
        panelWithTable = new Panel(table);
        addComponents(panelWithTable);
        createRowsAndColumns(variables1.size(),variables2.size(), variables1, variables2);
        numberOfVariableAdded = 0;
        // tableDataFiller.fillTableWithProperData();
        // Map<MultiKey, DataHolder> dataBeforeMap = new Converter().retrieveDataFromDataBase(table, list1, list2);
        // tableDataFiller.fillTableWithProperData(dataBeforeMap, table, numberOfVariableAdded);
    }

    private void removeRowAndColumn()
    {
        List<VariableDb> variables1 = VariableDao.readAllStateVariablesBySectorId(actualFirstSectorId);
        List<VariableDb> variables2 = VariableDao.readAllStateVariablesBySectorId(actualSecondSectorId);
        int size = (variables1.size() < variables2.size()) ? variables1.size() : variables2.size();
        numberOfVariableAdded--;
        if(numberOfVariableAdded <0 )
        {
            NotificationProducer.showInformationNotification("You cannot delete more variables than you added");
            numberOfVariableAdded = 0;
            return;
        }
        int loc_n = numberOfVariableAdded + size ;
        String propertyID = String.valueOf(loc_n);
        table.removeContainerProperty(propertyID);
        table.removeItem(loc_n);

        table.setPageLength(table.size());
    }

    private void addRowColumnTable()
    {
        addRowAndColumn();
    }

    private void addComponents()
    {
        panelWithTable = new Panel(table);
        panelWithTable.setSizeFull();
        addComponent(new HorizontalLayout(informationType));
        addComponent(panelWithTable);
    }

    public boolean isRowHeader(int j)
    {
        return j == 0;
    }
}
