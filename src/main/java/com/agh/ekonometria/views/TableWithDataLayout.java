/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.views;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class TableWithDataLayout extends VerticalLayout
{
    private Button researchPage = new Button("Zmień badanie");
    private Button modifySectors = new Button("Modyfikuj sektory");
    private Button saveData = new Button("Zapisz");
    private TableLayout tableLayout;

    public TableWithDataLayout(String researchId)
    {
        this.tableLayout = new TableLayout(researchId);
        init(researchId);
        initLayouts();
        addComponents(researchId);
    }

    private void init(String researchId)
    {
        researchPage.setIcon(FontAwesome.EXCHANGE);
        modifySectors.setIcon(FontAwesome.EDIT);
        saveData.setIcon(FontAwesome.SAVE);
        researchPage.addClickListener(new Button.ClickListener()
        {
            @Override public void buttonClick(Button.ClickEvent event)
            {
                removeAllComponents();
                addComponent(new ResearchLayout());
            }
        });
        modifySectors.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                removeAllComponents();
                addComponent(new ResearchDetailsLayout(researchId));
            }
        });
        saveData.addClickListener(new Button.ClickListener()
        {
            @Override public void buttonClick(Button.ClickEvent event)
            {
               tableLayout.saveData();
            }
        });
    }

    private void initLayouts()
    {

    }

    private void addComponents(String researchId)
    {
        addComponents(new HeaderLayout(), new SectorSelectLayout(researchId, tableLayout) ,tableLayout,new HorizontalLayout(researchPage,modifySectors, saveData));
    }
}
