package com.agh.ekonometria.views;

import com.agh.ekonometria.dao.SectorDao;
import com.agh.ekonometria.dao.VariableDao;
import com.agh.ekonometria.dao.model.SectorDb;
import com.agh.ekonometria.dao.model.VariableDb;
import com.agh.ekonometria.model.Sector;
import com.agh.ekonometria.views.buttons.RemoveVariableButton;
import com.agh.ekonometria.views.buttons.ShowVariableTimeSeriesButton;
import com.vaadin.event.Action;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddingSectorsAndVariablesLayout extends VerticalLayout
{
    List<Sector> sectors = new ArrayList<>();
    private static final Action ADD_ITEM_ACTION = new Action("Add item");

    private static final Action ADD_CATEGORY_ACTION = new Action("Add category");

    private static final Action REMOVE_ITEM_ACTION = new Action("Remove");

    private static final Action SHOW_TIMESERIES = new Action("Show time series");

    private TreeTable treeTable = new TreeTable();
    private Button addSector;
    private SectorAddWindow addingSectorsWindow = new SectorAddWindow();
    private int researchId;

    public AddingSectorsAndVariablesLayout(String researchId)
    {
        this.researchId = Integer.valueOf(researchId);
        init();
        initComponents();
        addComponents();

    }

    private void init()
    {
        setMargin(true);
    }

    private void initComponents()
    {
        addSector = new Button("Dodaj nowy sektor", e -> // Java 8
                addNewSector());
        addSector.setIcon(FontAwesome.PLUS_CIRCLE);

        initTable();
        addingSectorsWindow.addCloseListener(e -> {
            if (addingSectorsWindow.getName() != null && !addingSectorsWindow.getName().equals("")) {
                ButtonsPanel buttonsPanel = new ButtonsPanel();
                final Object sectorId = treeTable.addItem(new Object[]{addingSectorsWindow.getName(), new HorizontalLayout(buttonsPanel)}, null);
                Sector addedSector = new Sector().withName(addingSectorsWindow.getName()).withShortcutName(addingSectorsWindow.getShortcutName()).withDescription(addingSectorsWindow.getDescription());
                int sectorIdDb = SectorDao.create(addingSectorsWindow.getName(), addingSectorsWindow.getShortcutName(), addingSectorsWindow.getDescription(), researchId);
                sectors.add(addedSector);

                buttonsPanel.withSector(addedSector).withSectorIdInTable(sectorId).withTreeTable(treeTable).withSectors(sectors).withSectorIdDb(sectorIdDb).init();

                removeComponent(treeTable);
                addComponent(treeTable);
            }
        });
    }

    private void initTable()
    {
        treeTable.setSelectable(true);
        treeTable.setColumnHeaderMode(Table.ColumnHeaderMode.HIDDEN);
        treeTable.addContainerProperty("Sektory", String.class, null);
        treeTable.addContainerProperty("", HorizontalLayout.class, null);
        treeTable.setPageLength(treeTable.size());
        treeTable.setSizeFull();
        treeTable.setColumnExpandRatio("Sektory", 12);
        treeTable.setColumnExpandRatio("", 1);
        treeTable.addActionHandler(new Action.Handler()
        {
            @Override
            public void handleAction(final Action action, final Object sender,
                    final Object target)
            {
                if (action == ADD_ITEM_ACTION)
                {
                    // Create new item
                    final Object item = treeTable.addItem(new Object[] { "New Item", 0, new Date() }, null);
                    treeTable.setChildrenAllowed(item, false);
                    treeTable.setParent(item, target);
                }
                else if (action == ADD_CATEGORY_ACTION)
                {
                    final Object item = treeTable.addItem(new Object[] {
                            "New Category", 0, new Date() }, null);
                    treeTable.setParent(item, target);
                }
                else if (action == REMOVE_ITEM_ACTION)
                {
                    treeTable.removeItem(target);
                }

            }

            @Override
            public Action[] getActions(final Object target, final Object sender)
            {

                if (target == null)
                {
                    // Context menu in an empty space -> add a new main category
                    return new Action[] { ADD_CATEGORY_ACTION };

                }
                else if (treeTable.areChildrenAllowed(target))
                {
                    // Context menu for a category
                    return new Action[] { ADD_CATEGORY_ACTION, ADD_ITEM_ACTION,
                            REMOVE_ITEM_ACTION };

                }
                else
                {
                    // Context menu for an item
                    return new Action[] { REMOVE_ITEM_ACTION, SHOW_TIMESERIES };
                }
            }
        });

        List<SectorDb> sectorsFromDba = SectorDao.readAllByResearchId(researchId);
        for (SectorDb sectorDb : sectorsFromDba)
        {
            ButtonsPanel buttonsPanel = new ButtonsPanel();
            Object sectorId = treeTable.addItem(new Object[] { sectorDb.getName(), new HorizontalLayout(buttonsPanel) }, null);
            Sector addedSector = new Sector()
                    .withName(sectorDb.getName())
                    .withShortcutName(sectorDb.getAbbreviation())
                    .withDescription(sectorDb.getDescription())
                    .withVariables(VariableDao.readAllStateVariablesBySectorId(sectorDb.getId()));
            for (VariableDb variableDb : VariableDao.readAllStateVariablesBySectorId(sectorDb.getId()))
            {
                RemoveVariableButton removeVariableButton = new RemoveVariableButton();
                ShowVariableTimeSeriesButton variableTimeSeriesButton = new ShowVariableTimeSeriesButton();
                HorizontalLayout horizontalLayoutWithButton = new HorizontalLayout(removeVariableButton, variableTimeSeriesButton);
                horizontalLayoutWithButton.setSizeFull();
                horizontalLayoutWithButton.setComponentAlignment(removeVariableButton, Alignment.MIDDLE_LEFT);
                horizontalLayoutWithButton.setComponentAlignment(variableTimeSeriesButton, Alignment.MIDDLE_RIGHT);

                final Object variableId = treeTable.addItem(new Object[] { variableDb.getName(), horizontalLayoutWithButton }, null);
                removeVariableButton
                        .withSector(addedSector)
                        .withVariableIdDb(variableDb.getId())
                        .withTreeTable(treeTable)
                        .withVariableIdInTable(variableId)
                        .withVariable(variableDb);

                variableTimeSeriesButton
                        .withSector(addedSector)
                        .withVariableIdDb(variableDb.getId())
                        .withTreeTable(treeTable)
                        .withVariableIdInTable(variableId)
                        .withVariable(variableDb);
                treeTable.setParent(variableId, sectorId);
                treeTable.setChildrenAllowed(variableId, false);

            }
            sectors.add(addedSector);

            buttonsPanel.withSector(addedSector).withSectorIdInTable(sectorId).withTreeTable(treeTable).withSectors(sectors).withSectorIdDb(sectorDb.getId()).init();
        }

    }

    private void addNewSector()
    {
        UI.getCurrent().addWindow(addingSectorsWindow);
    }

    private void addComponents()
    {
        addComponent(new HorizontalLayout(addSector));
        addComponents(treeTable);
    }
}
