package com.agh.ekonometria;

/**
 * Created by Artur on 2015-09-06.
 */
public class Constants {

    public final static int HEADER_VALUE = -1;
    public final static int QUANTITY_VALUE = 0;

    public final static int FIVE_GRADE = 1;
    public final static int FOUR_GRADE = 2;
    public final static int SEVEN_GRADE = 3;
}
