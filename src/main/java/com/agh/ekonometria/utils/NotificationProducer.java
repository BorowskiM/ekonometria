/* Copyright 2015 Sabre Holdings */
package com.agh.ekonometria.utils;

import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class NotificationProducer
{
    public static void showInformationNotification(String message)
    {
        Notification notification = new Notification(message, "", Notification.Type.HUMANIZED_MESSAGE, true);
        notification.show(Page.getCurrent());
        notification.setDelayMsec(3000);
        notification.setPosition(Position.TOP_RIGHT);
    }
}
