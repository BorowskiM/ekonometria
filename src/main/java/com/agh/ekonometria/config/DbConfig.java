package com.agh.ekonometria.config;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by mtomanski on 16.06.15.
 */
public class DbConfig {

    static {
        initConnection();
    }

    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String url = "jdbc:mysql://mysql.agh.edu.pl/arturha1";
    private static final String username = "arturha1";
    private static final String password = "UoP1wvKf";

    private static Connection connection;

    public static void initConnection() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
//            connection.createStatement().execute("USE sql288878;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    // just an example method
    public static String exampleSelectQuery() {
        try {
            Statement stm = connection.createStatement();
            ResultSet resultSet = stm.executeQuery("Select name from Research where id = 1");
            resultSet.next();
            String name = resultSet.getString("name");
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
